import org.jetbrains.kotlin.gradle.dsl.JvmTarget

//Example
//https://github.com/apollographql/apollo-kotlin/blob/intellij-platform-gradle-plugin-v2.0.0/intellij-plugin/build.gradle.kts#L69
//https://github.com/apollographql/apollo-kotlin/blob/intellij-platform-gradle-plugin-v2.0.0/intellij-plugin/gradle.properties
fun properties(key: String) = project.findProperty(key).toString()

plugins {
	id("org.jetbrains.intellij.platform") version "2.2.0"
	kotlin("jvm") version "2.1.0"
}

group = "com.babestudios"
version = "0.4.0"

repositories {
	mavenCentral()
	intellijPlatform {
		defaultRepositories()
	}
}

intellijPlatform {
	pluginConfiguration {
		id = "com.babestudios.stringtemplate"
		name = "StringTemplate"
		description = "This is an IntelliJ plugin for using StringTemplate with specific highlighting rules."
		changeNotes = "Updated to support IntelliJ 2024.3"
		productDescriptor {
			releaseDate = "20241213"
			releaseVersion = "0.4.0"
		}
	}

}

sourceSets.getByName("main") {
	java.srcDir("src/main/java")
	java.srcDir("src/main/kotlin")
	java.srcDir("src/main/gen")
}

kotlin.compilerOptions{
	jvmTarget.set(JvmTarget.JVM_17)
}

dependencies {
	intellijPlatform {
		val type = properties("platformType")
		val version = properties("platformVersion")
		create(type, version)
		bundledPlugins(properties("platformBundledPlugins").split(',').map(String::trim).filter(String::isNotEmpty))
	}

	implementation("org.jetbrains.kotlin:kotlin-stdlib")
	implementation("org.antlr:ST4:4.3.4")
	testImplementation("io.kotest:kotest-runner-junit5:5.6.2")
	testImplementation("io.kotest:kotest-assertions-core:5.6.2")

}
