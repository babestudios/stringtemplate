// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateVariableDeclaration extends PsiElement {

  @Nullable
  StringTemplateByExpression getByExpression();

  @Nullable
  StringTemplateEqExpression getEqExpression();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @Nullable
  StringTemplatePropertyAccessor getPropertyAccessor();

  @Nullable
  StringTemplateTypeReference getTypeReference();

}
