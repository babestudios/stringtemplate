// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateRoot extends PsiElement {

  @NotNull
  List<StringTemplateClassDefinition> getClassDefinitionList();

  @NotNull
  List<StringTemplateConditionalIf> getConditionalIfList();

  @NotNull
  List<StringTemplateDeclaration> getDeclarationList();

  @NotNull
  List<StringTemplateFunction> getFunctionList();

  @NotNull
  List<StringTemplateImportDirective> getImportDirectiveList();

  @NotNull
  List<StringTemplatePackageDirective> getPackageDirectiveList();

}
