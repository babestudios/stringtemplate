// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.navigation.ItemPresentation;

public interface StringTemplateProperty extends StringTemplateNamedElement {

  @Nullable
  String getKey();

  @Nullable
  String getValue();

  @Nullable
  String getName();

  @NotNull
  PsiElement setName(@Nullable String newName);

  @Nullable
  PsiElement getNameIdentifier();

  @NotNull
  ItemPresentation getPresentation();

}
