// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateArgument extends PsiElement {

  @Nullable
  StringTemplateAnnotation getAnnotation();

  @Nullable
  StringTemplateConditionalIf getConditionalIf();

  @Nullable
  StringTemplateExpression getExpression();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @Nullable
  StringTemplateNamedArgument getNamedArgument();

}
