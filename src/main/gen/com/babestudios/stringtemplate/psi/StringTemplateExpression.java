// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateExpression extends PsiElement {

  @Nullable
  StringTemplateCodeBlockLambda getCodeBlockLambda();

  @Nullable
  StringTemplateFunctionCall getFunctionCall();

  @Nullable
  StringTemplateObject getObject();

  @Nullable
  StringTemplateVariableDeclaration getVariableDeclaration();

  @Nullable
  StringTemplateVariableReference getVariableReference();

  @Nullable
  StringTemplateWhenExpression getWhenExpression();

}
