// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateFunction extends PsiElement {

  @Nullable
  StringTemplateFunctionBody getFunctionBody();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @NotNull
  StringTemplateModifierList getModifierList();

  @NotNull
  StringTemplateParameterList getParameterList();

  @Nullable
  StringTemplateReturnType getReturnType();

}
