// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateWhenExpressionBody extends PsiElement {

  @NotNull
  List<StringTemplateConditionalIf> getConditionalIfList();

  @NotNull
  List<StringTemplateExpression> getExpressionList();

  @NotNull
  List<StringTemplateFunctionCall> getFunctionCallList();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

}
