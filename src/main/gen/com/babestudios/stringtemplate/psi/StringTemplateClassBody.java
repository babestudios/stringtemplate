// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateClassBody extends PsiElement {

  @NotNull
  List<StringTemplateClassDefinition> getClassDefinitionList();

  @NotNull
  List<StringTemplateConditionalIf> getConditionalIfList();

  @NotNull
  List<StringTemplateEnumEntries> getEnumEntriesList();

  @NotNull
  List<StringTemplateExplicitConstructorDefinition> getExplicitConstructorDefinitionList();

  @NotNull
  List<StringTemplateFunction> getFunctionList();

  @NotNull
  List<StringTemplateObject> getObjectList();

  @NotNull
  List<StringTemplateVariableDeclaration> getVariableDeclarationList();

}
