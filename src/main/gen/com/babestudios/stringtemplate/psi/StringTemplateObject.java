// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateObject extends PsiElement {

  @Nullable
  StringTemplateArgumentList getArgumentList();

  @Nullable
  StringTemplateClassBody getClassBody();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @NotNull
  StringTemplateModifierList getModifierList();

  @NotNull
  StringTemplateTypeParameterList getTypeParameterList();

}
