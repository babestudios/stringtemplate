// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class StringTemplateVisitor extends PsiElementVisitor {

  public void visitAnnotation(@NotNull StringTemplateAnnotation o) {
    visitPsiElement(o);
  }

  public void visitArgument(@NotNull StringTemplateArgument o) {
    visitPsiElement(o);
  }

  public void visitArgumentList(@NotNull StringTemplateArgumentList o) {
    visitPsiElement(o);
  }

  public void visitByExpression(@NotNull StringTemplateByExpression o) {
    visitPsiElement(o);
  }

  public void visitClassBody(@NotNull StringTemplateClassBody o) {
    visitPsiElement(o);
  }

  public void visitClassDefinition(@NotNull StringTemplateClassDefinition o) {
    visitPsiElement(o);
  }

  public void visitCodeBlockLambda(@NotNull StringTemplateCodeBlockLambda o) {
    visitPsiElement(o);
  }

  public void visitConditionalIf(@NotNull StringTemplateConditionalIf o) {
    visitPsiElement(o);
  }

  public void visitConstructorDefinition(@NotNull StringTemplateConstructorDefinition o) {
    visitPsiElement(o);
  }

  public void visitDeclaration(@NotNull StringTemplateDeclaration o) {
    visitPsiElement(o);
  }

  public void visitElvisExpression(@NotNull StringTemplateElvisExpression o) {
    visitPsiElement(o);
  }

  public void visitEmptyArgumentList(@NotNull StringTemplateEmptyArgumentList o) {
    visitPsiElement(o);
  }

  public void visitEmptyModifierList(@NotNull StringTemplateEmptyModifierList o) {
    visitPsiElement(o);
  }

  public void visitEmptyParameterList(@NotNull StringTemplateEmptyParameterList o) {
    visitPsiElement(o);
  }

  public void visitEmptyTypeParameterList(@NotNull StringTemplateEmptyTypeParameterList o) {
    visitPsiElement(o);
  }

  public void visitEnumEntries(@NotNull StringTemplateEnumEntries o) {
    visitPsiElement(o);
  }

  public void visitEnumEntry(@NotNull StringTemplateEnumEntry o) {
    visitPsiElement(o);
  }

  public void visitEqExpression(@NotNull StringTemplateEqExpression o) {
    visitPsiElement(o);
  }

  public void visitExplicitConstructorDefinition(@NotNull StringTemplateExplicitConstructorDefinition o) {
    visitPsiElement(o);
  }

  public void visitExpression(@NotNull StringTemplateExpression o) {
    visitPsiElement(o);
  }

  public void visitFunction(@NotNull StringTemplateFunction o) {
    visitPsiElement(o);
  }

  public void visitFunctionBody(@NotNull StringTemplateFunctionBody o) {
    visitPsiElement(o);
  }

  public void visitFunctionCall(@NotNull StringTemplateFunctionCall o) {
    visitPsiElement(o);
  }

  public void visitFunctionReference(@NotNull StringTemplateFunctionReference o) {
    visitPsiElement(o);
  }

  public void visitGenericType(@NotNull StringTemplateGenericType o) {
    visitPsiElement(o);
  }

  public void visitImplicitConstructorDefinition(@NotNull StringTemplateImplicitConstructorDefinition o) {
    visitPsiElement(o);
  }

  public void visitImportDirective(@NotNull StringTemplateImportDirective o) {
    visitPsiElement(o);
  }

  public void visitLambdaParameterList(@NotNull StringTemplateLambdaParameterList o) {
    visitPsiElement(o);
  }

  public void visitModelDefinition(@NotNull StringTemplateModelDefinition o) {
    visitPsiElement(o);
  }

  public void visitModifierList(@NotNull StringTemplateModifierList o) {
    visitPsiElement(o);
  }

  public void visitNamedArgument(@NotNull StringTemplateNamedArgument o) {
    visitPsiElement(o);
  }

  public void visitNonEmptyArgumentList(@NotNull StringTemplateNonEmptyArgumentList o) {
    visitPsiElement(o);
  }

  public void visitNonEmptyModifierList(@NotNull StringTemplateNonEmptyModifierList o) {
    visitPsiElement(o);
  }

  public void visitNonEmptyParameterList(@NotNull StringTemplateNonEmptyParameterList o) {
    visitPsiElement(o);
  }

  public void visitNonEmptyTypeParameterList(@NotNull StringTemplateNonEmptyTypeParameterList o) {
    visitPsiElement(o);
  }

  public void visitObject(@NotNull StringTemplateObject o) {
    visitPsiElement(o);
  }

  public void visitPackageDirective(@NotNull StringTemplatePackageDirective o) {
    visitPsiElement(o);
  }

  public void visitParameter(@NotNull StringTemplateParameter o) {
    visitPsiElement(o);
  }

  public void visitParameterList(@NotNull StringTemplateParameterList o) {
    visitPsiElement(o);
  }

  public void visitProperty(@NotNull StringTemplateProperty o) {
    visitNamedElement(o);
  }

  public void visitPropertyAccessor(@NotNull StringTemplatePropertyAccessor o) {
    visitPsiElement(o);
  }

  public void visitReturnType(@NotNull StringTemplateReturnType o) {
    visitPsiElement(o);
  }

  public void visitRoot(@NotNull StringTemplateRoot o) {
    visitPsiElement(o);
  }

  public void visitSuperTypeList(@NotNull StringTemplateSuperTypeList o) {
    visitPsiElement(o);
  }

  public void visitTypeDefinition(@NotNull StringTemplateTypeDefinition o) {
    visitPsiElement(o);
  }

  public void visitTypeParameter(@NotNull StringTemplateTypeParameter o) {
    visitPsiElement(o);
  }

  public void visitTypeParameterList(@NotNull StringTemplateTypeParameterList o) {
    visitPsiElement(o);
  }

  public void visitTypeReference(@NotNull StringTemplateTypeReference o) {
    visitPsiElement(o);
  }

  public void visitTypealiasDeclaration(@NotNull StringTemplateTypealiasDeclaration o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclaration(@NotNull StringTemplateVariableDeclaration o) {
    visitPsiElement(o);
  }

  public void visitVariableReference(@NotNull StringTemplateVariableReference o) {
    visitPsiElement(o);
  }

  public void visitWhenExpression(@NotNull StringTemplateWhenExpression o) {
    visitPsiElement(o);
  }

  public void visitWhenExpressionBody(@NotNull StringTemplateWhenExpressionBody o) {
    visitPsiElement(o);
  }

  public void visitNamedElement(@NotNull StringTemplateNamedElement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
