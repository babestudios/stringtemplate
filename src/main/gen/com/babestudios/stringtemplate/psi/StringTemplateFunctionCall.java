// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateFunctionCall extends PsiElement {

  @NotNull
  StringTemplateArgumentList getArgumentList();

  @Nullable
  StringTemplateCodeBlockLambda getCodeBlockLambda();

  @NotNull
  List<StringTemplateConditionalIf> getConditionalIfList();

  @Nullable
  StringTemplateElvisExpression getElvisExpression();

  @NotNull
  List<StringTemplateExpression> getExpressionList();

  @NotNull
  List<StringTemplateFunctionCall> getFunctionCallList();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

}
