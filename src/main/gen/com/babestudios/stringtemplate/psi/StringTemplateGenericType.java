// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateGenericType extends PsiElement {

  @NotNull
  StringTemplateNonEmptyTypeParameterList getNonEmptyTypeParameterList();

}
