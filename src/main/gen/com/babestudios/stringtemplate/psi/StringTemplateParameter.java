// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateParameter extends PsiElement {

  @NotNull
  List<StringTemplateAnnotation> getAnnotationList();

  @Nullable
  StringTemplateConditionalIf getConditionalIf();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @NotNull
  StringTemplateModifierList getModifierList();

  @NotNull
  StringTemplateTypeReference getTypeReference();

}
