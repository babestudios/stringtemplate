// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.babestudios.stringtemplate.psi.impl.*;

public interface StringTemplateTypes {

  IElementType ANNOTATION = new StringTemplateElementType("ANNOTATION");
  IElementType ARGUMENT = new StringTemplateElementType("ARGUMENT");
  IElementType ARGUMENT_LIST = new StringTemplateElementType("ARGUMENT_LIST");
  IElementType BY_EXPRESSION = new StringTemplateElementType("BY_EXPRESSION");
  IElementType CLASS_BODY = new StringTemplateElementType("CLASS_BODY");
  IElementType CLASS_DEFINITION = new StringTemplateElementType("CLASS_DEFINITION");
  IElementType CODE_BLOCK_LAMBDA = new StringTemplateElementType("CODE_BLOCK_LAMBDA");
  IElementType CONDITIONAL_IF = new StringTemplateElementType("CONDITIONAL_IF");
  IElementType CONSTRUCTOR_DEFINITION = new StringTemplateElementType("CONSTRUCTOR_DEFINITION");
  IElementType DECLARATION = new StringTemplateElementType("DECLARATION");
  IElementType ELVIS_EXPRESSION = new StringTemplateElementType("ELVIS_EXPRESSION");
  IElementType EMPTY_ARGUMENT_LIST = new StringTemplateElementType("EMPTY_ARGUMENT_LIST");
  IElementType EMPTY_MODIFIER_LIST = new StringTemplateElementType("EMPTY_MODIFIER_LIST");
  IElementType EMPTY_PARAMETER_LIST = new StringTemplateElementType("EMPTY_PARAMETER_LIST");
  IElementType EMPTY_TYPE_PARAMETER_LIST = new StringTemplateElementType("EMPTY_TYPE_PARAMETER_LIST");
  IElementType ENUM_ENTRIES = new StringTemplateElementType("ENUM_ENTRIES");
  IElementType ENUM_ENTRY = new StringTemplateElementType("ENUM_ENTRY");
  IElementType EQ_EXPRESSION = new StringTemplateElementType("EQ_EXPRESSION");
  IElementType EXPLICIT_CONSTRUCTOR_DEFINITION = new StringTemplateElementType("EXPLICIT_CONSTRUCTOR_DEFINITION");
  IElementType EXPRESSION = new StringTemplateElementType("EXPRESSION");
  IElementType FUNCTION = new StringTemplateElementType("FUNCTION");
  IElementType FUNCTION_BODY = new StringTemplateElementType("FUNCTION_BODY");
  IElementType FUNCTION_CALL = new StringTemplateElementType("FUNCTION_CALL");
  IElementType FUNCTION_REFERENCE = new StringTemplateElementType("FUNCTION_REFERENCE");
  IElementType GENERIC_TYPE = new StringTemplateElementType("GENERIC_TYPE");
  IElementType IMPLICIT_CONSTRUCTOR_DEFINITION = new StringTemplateElementType("IMPLICIT_CONSTRUCTOR_DEFINITION");
  IElementType IMPORT_DIRECTIVE = new StringTemplateElementType("IMPORT_DIRECTIVE");
  IElementType LAMBDA_PARAMETER_LIST = new StringTemplateElementType("LAMBDA_PARAMETER_LIST");
  IElementType MODEL_DEFINITION = new StringTemplateElementType("MODEL_DEFINITION");
  IElementType MODIFIER_LIST = new StringTemplateElementType("MODIFIER_LIST");
  IElementType NAMED_ARGUMENT = new StringTemplateElementType("NAMED_ARGUMENT");
  IElementType NON_EMPTY_ARGUMENT_LIST = new StringTemplateElementType("NON_EMPTY_ARGUMENT_LIST");
  IElementType NON_EMPTY_MODIFIER_LIST = new StringTemplateElementType("NON_EMPTY_MODIFIER_LIST");
  IElementType NON_EMPTY_PARAMETER_LIST = new StringTemplateElementType("NON_EMPTY_PARAMETER_LIST");
  IElementType NON_EMPTY_TYPE_PARAMETER_LIST = new StringTemplateElementType("NON_EMPTY_TYPE_PARAMETER_LIST");
  IElementType OBJECT = new StringTemplateElementType("OBJECT");
  IElementType PACKAGE_DIRECTIVE = new StringTemplateElementType("PACKAGE_DIRECTIVE");
  IElementType PARAMETER = new StringTemplateElementType("PARAMETER");
  IElementType PARAMETER_LIST = new StringTemplateElementType("PARAMETER_LIST");
  IElementType PROPERTY = new StringTemplateElementType("PROPERTY");
  IElementType PROPERTY_ACCESSOR = new StringTemplateElementType("PROPERTY_ACCESSOR");
  IElementType RETURN_TYPE = new StringTemplateElementType("RETURN_TYPE");
  IElementType ROOT = new StringTemplateElementType("ROOT");
  IElementType SUPER_TYPE_LIST = new StringTemplateElementType("SUPER_TYPE_LIST");
  IElementType TYPEALIAS_DECLARATION = new StringTemplateElementType("TYPEALIAS_DECLARATION");
  IElementType TYPE_DEFINITION = new StringTemplateElementType("TYPE_DEFINITION");
  IElementType TYPE_PARAMETER = new StringTemplateElementType("TYPE_PARAMETER");
  IElementType TYPE_PARAMETER_LIST = new StringTemplateElementType("TYPE_PARAMETER_LIST");
  IElementType TYPE_REFERENCE = new StringTemplateElementType("TYPE_REFERENCE");
  IElementType VARIABLE_DECLARATION = new StringTemplateElementType("VARIABLE_DECLARATION");
  IElementType VARIABLE_REFERENCE = new StringTemplateElementType("VARIABLE_REFERENCE");
  IElementType WHEN_EXPRESSION = new StringTemplateElementType("WHEN_EXPRESSION");
  IElementType WHEN_EXPRESSION_BODY = new StringTemplateElementType("WHEN_EXPRESSION_BODY");

  IElementType COMMENT = new StringTemplateTokenType("COMMENT");
  IElementType CONDITIONAL = new StringTemplateTokenType("CONDITIONAL");
  IElementType CONDITIONAL_IF_END = new StringTemplateTokenType("CONDITIONAL_IF_END");
  IElementType CONDITIONAL_IF_START = new StringTemplateTokenType("CONDITIONAL_IF_START");
  IElementType CRLF = new StringTemplateTokenType("CRLF");
  IElementType DOT_QUALIFIED_EXPRESSION_PART_LITERAL = new StringTemplateTokenType("DOT_QUALIFIED_EXPRESSION_PART_LITERAL");
  IElementType FLOAT_CONSTANT = new StringTemplateTokenType("FLOAT_CONSTANT");
  IElementType HEXA_CONSTANT = new StringTemplateTokenType("HEXA_CONSTANT");
  IElementType IDENTIFIER = new StringTemplateTokenType("identifier");
  IElementType INTEGER_CONSTANT = new StringTemplateTokenType("INTEGER_CONSTANT");
  IElementType KEY = new StringTemplateTokenType("KEY");
  IElementType KW_ABSTRACT = new StringTemplateTokenType("abstract");
  IElementType KW_ACTUAL = new StringTemplateTokenType("actual");
  IElementType KW_ANNOTATION = new StringTemplateTokenType("annotation");
  IElementType KW_AS = new StringTemplateTokenType("as");
  IElementType KW_BREAK = new StringTemplateTokenType("break");
  IElementType KW_BY = new StringTemplateTokenType("by");
  IElementType KW_CATCH = new StringTemplateTokenType("catch");
  IElementType KW_CLASS = new StringTemplateTokenType("class");
  IElementType KW_COMPANION = new StringTemplateTokenType("companion");
  IElementType KW_CONST = new StringTemplateTokenType("const");
  IElementType KW_CONSTRUCTOR = new StringTemplateTokenType("constructor");
  IElementType KW_CONTEXT = new StringTemplateTokenType("context");
  IElementType KW_CONTINUE = new StringTemplateTokenType("continue");
  IElementType KW_CONTRACT = new StringTemplateTokenType("contract");
  IElementType KW_CROSSINLINE = new StringTemplateTokenType("crossinline");
  IElementType KW_DATA = new StringTemplateTokenType("data");
  IElementType KW_DELEGATE = new StringTemplateTokenType("delegate");
  IElementType KW_DO = new StringTemplateTokenType("do");
  IElementType KW_DYNAMIC = new StringTemplateTokenType("dynamic");
  IElementType KW_ELSE = new StringTemplateTokenType("else");
  IElementType KW_ENUM = new StringTemplateTokenType("enum");
  IElementType KW_EXPECT = new StringTemplateTokenType("expect");
  IElementType KW_EXTERNAL = new StringTemplateTokenType("external");
  IElementType KW_FALSE = new StringTemplateTokenType("false");
  IElementType KW_FIELD = new StringTemplateTokenType("field");
  IElementType KW_FILE = new StringTemplateTokenType("file");
  IElementType KW_FINAL = new StringTemplateTokenType("final");
  IElementType KW_FINALLY = new StringTemplateTokenType("finally");
  IElementType KW_FOR = new StringTemplateTokenType("for");
  IElementType KW_FUN = new StringTemplateTokenType("fun");
  IElementType KW_GET = new StringTemplateTokenType("get");
  IElementType KW_HEADER = new StringTemplateTokenType("header");
  IElementType KW_IF = new StringTemplateTokenType("if");
  IElementType KW_IMPL = new StringTemplateTokenType("impl");
  IElementType KW_IMPORT = new StringTemplateTokenType("import");
  IElementType KW_IN = new StringTemplateTokenType("in");
  IElementType KW_INFIX = new StringTemplateTokenType("infix");
  IElementType KW_INIT = new StringTemplateTokenType("init");
  IElementType KW_INLINE = new StringTemplateTokenType("inline");
  IElementType KW_INNER = new StringTemplateTokenType("inner");
  IElementType KW_INTERFACE = new StringTemplateTokenType("interface");
  IElementType KW_INTERNAL = new StringTemplateTokenType("internal");
  IElementType KW_IS = new StringTemplateTokenType("is");
  IElementType KW_LATEINIT = new StringTemplateTokenType("lateinit");
  IElementType KW_NOINLINE = new StringTemplateTokenType("noinline");
  IElementType KW_NULL = new StringTemplateTokenType("null");
  IElementType KW_OBJECT = new StringTemplateTokenType("object");
  IElementType KW_OPEN = new StringTemplateTokenType("open");
  IElementType KW_OPERATOR = new StringTemplateTokenType("operator");
  IElementType KW_OUT = new StringTemplateTokenType("out");
  IElementType KW_OVERRIDE = new StringTemplateTokenType("override");
  IElementType KW_PACKAGE = new StringTemplateTokenType("package");
  IElementType KW_PARAM = new StringTemplateTokenType("param");
  IElementType KW_PRIVATE = new StringTemplateTokenType("private");
  IElementType KW_PROPERTY = new StringTemplateTokenType("property");
  IElementType KW_PROTECTED = new StringTemplateTokenType("protected");
  IElementType KW_PUBLIC = new StringTemplateTokenType("public");
  IElementType KW_RECEIVER = new StringTemplateTokenType("receiver");
  IElementType KW_REIFIED = new StringTemplateTokenType("reified");
  IElementType KW_RETURN = new StringTemplateTokenType("return");
  IElementType KW_SEALED = new StringTemplateTokenType("sealed");
  IElementType KW_SET = new StringTemplateTokenType("set");
  IElementType KW_SETPARAM = new StringTemplateTokenType("setparam");
  IElementType KW_STATIC = new StringTemplateTokenType("static");
  IElementType KW_SUPER = new StringTemplateTokenType("super");
  IElementType KW_SUSPEND = new StringTemplateTokenType("suspend");
  IElementType KW_SYNCHRONIZED = new StringTemplateTokenType("synchronized");
  IElementType KW_TAILREC = new StringTemplateTokenType("tailrec");
  IElementType KW_THIS = new StringTemplateTokenType("this");
  IElementType KW_THROW = new StringTemplateTokenType("throw");
  IElementType KW_TRANSIENT = new StringTemplateTokenType("transient");
  IElementType KW_TRUE = new StringTemplateTokenType("true");
  IElementType KW_TRY = new StringTemplateTokenType("try");
  IElementType KW_TYPE_ALIAS = new StringTemplateTokenType("typealias");
  IElementType KW_VAL = new StringTemplateTokenType("val");
  IElementType KW_VALUE = new StringTemplateTokenType("value");
  IElementType KW_VAR = new StringTemplateTokenType("var");
  IElementType KW_VARARG = new StringTemplateTokenType("vararg");
  IElementType KW_WHEN = new StringTemplateTokenType("when");
  IElementType KW_WHERE = new StringTemplateTokenType("where");
  IElementType KW_WHILE = new StringTemplateTokenType("while");
  IElementType LONG_CONSTANT = new StringTemplateTokenType("LONG_CONSTANT");
  IElementType MODEL_CLOSING = new StringTemplateTokenType("MODEL_CLOSING");
  IElementType MODEL_FORMATTER = new StringTemplateTokenType("MODEL_FORMATTER");
  IElementType MODEL_STATEMENT = new StringTemplateTokenType("MODEL_STATEMENT");
  IElementType NL = new StringTemplateTokenType("new line");
  IElementType SEPARATOR = new StringTemplateTokenType("SEPARATOR");
  IElementType STRING_LITERAL = new StringTemplateTokenType("STRING_LITERAL");
  IElementType T_AND = new StringTemplateTokenType("&");
  IElementType T_ANDAND = new StringTemplateTokenType("&&");
  IElementType T_ARROW = new StringTemplateTokenType("->");
  IElementType T_AT = new StringTemplateTokenType("@");
  IElementType T_COLON = new StringTemplateTokenType(":");
  IElementType T_COLONCOLON = new StringTemplateTokenType("::");
  IElementType T_COMMA = new StringTemplateTokenType(",");
  IElementType T_DIV = new StringTemplateTokenType("/");
  IElementType T_DIVEQ = new StringTemplateTokenType("/=");
  IElementType T_DOT = new StringTemplateTokenType(".");
  IElementType T_DOUBLE_ARROW = new StringTemplateTokenType("=>");
  IElementType T_DOUBLE_SEMICOLON = new StringTemplateTokenType(";;");
  IElementType T_ELVIS = new StringTemplateTokenType("?:");
  IElementType T_EQ = new StringTemplateTokenType("=");
  IElementType T_EQEQEQ = new StringTemplateTokenType("===");
  IElementType T_EXCL = new StringTemplateTokenType("!");
  IElementType T_EXCLEQ = new StringTemplateTokenType("!=");
  IElementType T_EXCLEXCL = new StringTemplateTokenType("!!");
  IElementType T_GT = new StringTemplateTokenType(">");
  IElementType T_GTEQ = new StringTemplateTokenType(">=");
  IElementType T_HASH = new StringTemplateTokenType("#");
  IElementType T_LBRACE = new StringTemplateTokenType("LBRACE");
  IElementType T_LBRACK = new StringTemplateTokenType("[");
  IElementType T_LPAREN = new StringTemplateTokenType("(");
  IElementType T_LT = new StringTemplateTokenType("<");
  IElementType T_LTEQ = new StringTemplateTokenType("<=");
  IElementType T_MINUS = new StringTemplateTokenType("-");
  IElementType T_MINUSEQ = new StringTemplateTokenType("-=");
  IElementType T_MINUSMINUS = new StringTemplateTokenType("--");
  IElementType T_MUL = new StringTemplateTokenType("*");
  IElementType T_MULTEQ = new StringTemplateTokenType("*=");
  IElementType T_NOT_IN = new StringTemplateTokenType("!in");
  IElementType T_NOT_IS = new StringTemplateTokenType("!is");
  IElementType T_OROR = new StringTemplateTokenType("||");
  IElementType T_PERC = new StringTemplateTokenType("%");
  IElementType T_PERCEQ = new StringTemplateTokenType("%=");
  IElementType T_PLUS = new StringTemplateTokenType("+");
  IElementType T_PLUSEQ = new StringTemplateTokenType("+=");
  IElementType T_PLUSPPLUS = new StringTemplateTokenType("++");
  IElementType T_QUEST = new StringTemplateTokenType("?");
  IElementType T_RANGE = new StringTemplateTokenType("..");
  IElementType T_RANGE_UNTIL = new StringTemplateTokenType("..<");
  IElementType T_RBRACE = new StringTemplateTokenType("RBRACE");
  IElementType T_RBRACK = new StringTemplateTokenType("]");
  IElementType T_RPAREN = new StringTemplateTokenType(")");
  IElementType T_SAFE_ACCESS = new StringTemplateTokenType("?.");
  IElementType T_SEMICOLON = new StringTemplateTokenType(";");
  IElementType VALUE = new StringTemplateTokenType("VALUE");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ANNOTATION) {
        return new StringTemplateAnnotationImpl(node);
      }
      else if (type == ARGUMENT) {
        return new StringTemplateArgumentImpl(node);
      }
      else if (type == ARGUMENT_LIST) {
        return new StringTemplateArgumentListImpl(node);
      }
      else if (type == BY_EXPRESSION) {
        return new StringTemplateByExpressionImpl(node);
      }
      else if (type == CLASS_BODY) {
        return new StringTemplateClassBodyImpl(node);
      }
      else if (type == CLASS_DEFINITION) {
        return new StringTemplateClassDefinitionImpl(node);
      }
      else if (type == CODE_BLOCK_LAMBDA) {
        return new StringTemplateCodeBlockLambdaImpl(node);
      }
      else if (type == CONDITIONAL_IF) {
        return new StringTemplateConditionalIfImpl(node);
      }
      else if (type == CONSTRUCTOR_DEFINITION) {
        return new StringTemplateConstructorDefinitionImpl(node);
      }
      else if (type == DECLARATION) {
        return new StringTemplateDeclarationImpl(node);
      }
      else if (type == ELVIS_EXPRESSION) {
        return new StringTemplateElvisExpressionImpl(node);
      }
      else if (type == EMPTY_ARGUMENT_LIST) {
        return new StringTemplateEmptyArgumentListImpl(node);
      }
      else if (type == EMPTY_MODIFIER_LIST) {
        return new StringTemplateEmptyModifierListImpl(node);
      }
      else if (type == EMPTY_PARAMETER_LIST) {
        return new StringTemplateEmptyParameterListImpl(node);
      }
      else if (type == EMPTY_TYPE_PARAMETER_LIST) {
        return new StringTemplateEmptyTypeParameterListImpl(node);
      }
      else if (type == ENUM_ENTRIES) {
        return new StringTemplateEnumEntriesImpl(node);
      }
      else if (type == ENUM_ENTRY) {
        return new StringTemplateEnumEntryImpl(node);
      }
      else if (type == EQ_EXPRESSION) {
        return new StringTemplateEqExpressionImpl(node);
      }
      else if (type == EXPLICIT_CONSTRUCTOR_DEFINITION) {
        return new StringTemplateExplicitConstructorDefinitionImpl(node);
      }
      else if (type == EXPRESSION) {
        return new StringTemplateExpressionImpl(node);
      }
      else if (type == FUNCTION) {
        return new StringTemplateFunctionImpl(node);
      }
      else if (type == FUNCTION_BODY) {
        return new StringTemplateFunctionBodyImpl(node);
      }
      else if (type == FUNCTION_CALL) {
        return new StringTemplateFunctionCallImpl(node);
      }
      else if (type == FUNCTION_REFERENCE) {
        return new StringTemplateFunctionReferenceImpl(node);
      }
      else if (type == GENERIC_TYPE) {
        return new StringTemplateGenericTypeImpl(node);
      }
      else if (type == IMPLICIT_CONSTRUCTOR_DEFINITION) {
        return new StringTemplateImplicitConstructorDefinitionImpl(node);
      }
      else if (type == IMPORT_DIRECTIVE) {
        return new StringTemplateImportDirectiveImpl(node);
      }
      else if (type == LAMBDA_PARAMETER_LIST) {
        return new StringTemplateLambdaParameterListImpl(node);
      }
      else if (type == MODEL_DEFINITION) {
        return new StringTemplateModelDefinitionImpl(node);
      }
      else if (type == MODIFIER_LIST) {
        return new StringTemplateModifierListImpl(node);
      }
      else if (type == NAMED_ARGUMENT) {
        return new StringTemplateNamedArgumentImpl(node);
      }
      else if (type == NON_EMPTY_ARGUMENT_LIST) {
        return new StringTemplateNonEmptyArgumentListImpl(node);
      }
      else if (type == NON_EMPTY_MODIFIER_LIST) {
        return new StringTemplateNonEmptyModifierListImpl(node);
      }
      else if (type == NON_EMPTY_PARAMETER_LIST) {
        return new StringTemplateNonEmptyParameterListImpl(node);
      }
      else if (type == NON_EMPTY_TYPE_PARAMETER_LIST) {
        return new StringTemplateNonEmptyTypeParameterListImpl(node);
      }
      else if (type == OBJECT) {
        return new StringTemplateObjectImpl(node);
      }
      else if (type == PACKAGE_DIRECTIVE) {
        return new StringTemplatePackageDirectiveImpl(node);
      }
      else if (type == PARAMETER) {
        return new StringTemplateParameterImpl(node);
      }
      else if (type == PARAMETER_LIST) {
        return new StringTemplateParameterListImpl(node);
      }
      else if (type == PROPERTY) {
        return new StringTemplatePropertyImpl(node);
      }
      else if (type == PROPERTY_ACCESSOR) {
        return new StringTemplatePropertyAccessorImpl(node);
      }
      else if (type == RETURN_TYPE) {
        return new StringTemplateReturnTypeImpl(node);
      }
      else if (type == ROOT) {
        return new StringTemplateRootImpl(node);
      }
      else if (type == SUPER_TYPE_LIST) {
        return new StringTemplateSuperTypeListImpl(node);
      }
      else if (type == TYPEALIAS_DECLARATION) {
        return new StringTemplateTypealiasDeclarationImpl(node);
      }
      else if (type == TYPE_DEFINITION) {
        return new StringTemplateTypeDefinitionImpl(node);
      }
      else if (type == TYPE_PARAMETER) {
        return new StringTemplateTypeParameterImpl(node);
      }
      else if (type == TYPE_PARAMETER_LIST) {
        return new StringTemplateTypeParameterListImpl(node);
      }
      else if (type == TYPE_REFERENCE) {
        return new StringTemplateTypeReferenceImpl(node);
      }
      else if (type == VARIABLE_DECLARATION) {
        return new StringTemplateVariableDeclarationImpl(node);
      }
      else if (type == VARIABLE_REFERENCE) {
        return new StringTemplateVariableReferenceImpl(node);
      }
      else if (type == WHEN_EXPRESSION) {
        return new StringTemplateWhenExpressionImpl(node);
      }
      else if (type == WHEN_EXPRESSION_BODY) {
        return new StringTemplateWhenExpressionBodyImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
