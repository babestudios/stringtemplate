// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface StringTemplateConstructorDefinition extends PsiElement {

  @Nullable
  StringTemplateAnnotation getAnnotation();

  @Nullable
  StringTemplateExplicitConstructorDefinition getExplicitConstructorDefinition();

  @Nullable
  StringTemplateImplicitConstructorDefinition getImplicitConstructorDefinition();

  @NotNull
  List<StringTemplateModelDefinition> getModelDefinitionList();

  @Nullable
  StringTemplateModifierList getModifierList();

}
