// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateRootImpl extends ASTWrapperPsiElement implements StringTemplateRoot {

  public StringTemplateRootImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitRoot(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<StringTemplateClassDefinition> getClassDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateClassDefinition.class);
  }

  @Override
  @NotNull
  public List<StringTemplateConditionalIf> getConditionalIfList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateConditionalIf.class);
  }

  @Override
  @NotNull
  public List<StringTemplateDeclaration> getDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateDeclaration.class);
  }

  @Override
  @NotNull
  public List<StringTemplateFunction> getFunctionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateFunction.class);
  }

  @Override
  @NotNull
  public List<StringTemplateImportDirective> getImportDirectiveList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateImportDirective.class);
  }

  @Override
  @NotNull
  public List<StringTemplatePackageDirective> getPackageDirectiveList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplatePackageDirective.class);
  }

}
