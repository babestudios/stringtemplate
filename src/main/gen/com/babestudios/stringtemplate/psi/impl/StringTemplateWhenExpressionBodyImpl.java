// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateWhenExpressionBodyImpl extends ASTWrapperPsiElement implements StringTemplateWhenExpressionBody {

  public StringTemplateWhenExpressionBodyImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitWhenExpressionBody(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<StringTemplateConditionalIf> getConditionalIfList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateConditionalIf.class);
  }

  @Override
  @NotNull
  public List<StringTemplateExpression> getExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateExpression.class);
  }

  @Override
  @NotNull
  public List<StringTemplateFunctionCall> getFunctionCallList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateFunctionCall.class);
  }

  @Override
  @NotNull
  public List<StringTemplateModelDefinition> getModelDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateModelDefinition.class);
  }

}
