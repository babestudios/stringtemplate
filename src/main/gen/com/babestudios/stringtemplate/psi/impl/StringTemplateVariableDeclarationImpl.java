// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateVariableDeclarationImpl extends ASTWrapperPsiElement implements StringTemplateVariableDeclaration {

  public StringTemplateVariableDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitVariableDeclaration(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public StringTemplateByExpression getByExpression() {
    return findChildByClass(StringTemplateByExpression.class);
  }

  @Override
  @Nullable
  public StringTemplateEqExpression getEqExpression() {
    return findChildByClass(StringTemplateEqExpression.class);
  }

  @Override
  @NotNull
  public List<StringTemplateModelDefinition> getModelDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateModelDefinition.class);
  }

  @Override
  @Nullable
  public StringTemplatePropertyAccessor getPropertyAccessor() {
    return findChildByClass(StringTemplatePropertyAccessor.class);
  }

  @Override
  @Nullable
  public StringTemplateTypeReference getTypeReference() {
    return findChildByClass(StringTemplateTypeReference.class);
  }

}
