// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateWhenExpressionImpl extends ASTWrapperPsiElement implements StringTemplateWhenExpression {

  public StringTemplateWhenExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitWhenExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public StringTemplateArgument getArgument() {
    return findNotNullChildByClass(StringTemplateArgument.class);
  }

  @Override
  @NotNull
  public List<StringTemplateWhenExpressionBody> getWhenExpressionBodyList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateWhenExpressionBody.class);
  }

}
