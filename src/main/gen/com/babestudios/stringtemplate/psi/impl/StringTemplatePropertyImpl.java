// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.babestudios.stringtemplate.psi.*;
import com.intellij.navigation.ItemPresentation;

public class StringTemplatePropertyImpl extends StringTemplateNamedElementImpl implements StringTemplateProperty {

  public StringTemplatePropertyImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitProperty(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public String getKey() {
    return StringTemplatePsiImplUtil.getKey(this);
  }

  @Override
  @Nullable
  public String getValue() {
    return StringTemplatePsiImplUtil.getValue(this);
  }

  @Override
  @Nullable
  public String getName() {
    return StringTemplatePsiImplUtil.getName(this);
  }

  @Override
  @NotNull
  public PsiElement setName(@Nullable String newName) {
    return StringTemplatePsiImplUtil.setName(this, newName);
  }

  @Override
  @Nullable
  public PsiElement getNameIdentifier() {
    return StringTemplatePsiImplUtil.getNameIdentifier(this);
  }

  @Override
  @NotNull
  public ItemPresentation getPresentation() {
    return StringTemplatePsiImplUtil.getPresentation(this);
  }

}
