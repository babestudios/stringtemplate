// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateExpressionImpl extends ASTWrapperPsiElement implements StringTemplateExpression {

  public StringTemplateExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public StringTemplateCodeBlockLambda getCodeBlockLambda() {
    return findChildByClass(StringTemplateCodeBlockLambda.class);
  }

  @Override
  @Nullable
  public StringTemplateFunctionCall getFunctionCall() {
    return findChildByClass(StringTemplateFunctionCall.class);
  }

  @Override
  @Nullable
  public StringTemplateObject getObject() {
    return findChildByClass(StringTemplateObject.class);
  }

  @Override
  @Nullable
  public StringTemplateVariableDeclaration getVariableDeclaration() {
    return findChildByClass(StringTemplateVariableDeclaration.class);
  }

  @Override
  @Nullable
  public StringTemplateVariableReference getVariableReference() {
    return findChildByClass(StringTemplateVariableReference.class);
  }

  @Override
  @Nullable
  public StringTemplateWhenExpression getWhenExpression() {
    return findChildByClass(StringTemplateWhenExpression.class);
  }

}
