// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateObjectImpl extends ASTWrapperPsiElement implements StringTemplateObject {

  public StringTemplateObjectImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitObject(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public StringTemplateArgumentList getArgumentList() {
    return findChildByClass(StringTemplateArgumentList.class);
  }

  @Override
  @Nullable
  public StringTemplateClassBody getClassBody() {
    return findChildByClass(StringTemplateClassBody.class);
  }

  @Override
  @NotNull
  public List<StringTemplateModelDefinition> getModelDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateModelDefinition.class);
  }

  @Override
  @NotNull
  public StringTemplateModifierList getModifierList() {
    return findNotNullChildByClass(StringTemplateModifierList.class);
  }

  @Override
  @NotNull
  public StringTemplateTypeParameterList getTypeParameterList() {
    return findNotNullChildByClass(StringTemplateTypeParameterList.class);
  }

}
