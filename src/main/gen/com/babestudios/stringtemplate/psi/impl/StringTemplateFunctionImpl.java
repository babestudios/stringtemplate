// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateFunctionImpl extends ASTWrapperPsiElement implements StringTemplateFunction {

  public StringTemplateFunctionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitFunction(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public StringTemplateFunctionBody getFunctionBody() {
    return findChildByClass(StringTemplateFunctionBody.class);
  }

  @Override
  @NotNull
  public List<StringTemplateModelDefinition> getModelDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateModelDefinition.class);
  }

  @Override
  @NotNull
  public StringTemplateModifierList getModifierList() {
    return findNotNullChildByClass(StringTemplateModifierList.class);
  }

  @Override
  @NotNull
  public StringTemplateParameterList getParameterList() {
    return findNotNullChildByClass(StringTemplateParameterList.class);
  }

  @Override
  @Nullable
  public StringTemplateReturnType getReturnType() {
    return findChildByClass(StringTemplateReturnType.class);
  }

}
