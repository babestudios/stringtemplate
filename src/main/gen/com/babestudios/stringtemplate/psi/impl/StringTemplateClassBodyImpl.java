// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.babestudios.stringtemplate.psi.*;

public class StringTemplateClassBodyImpl extends ASTWrapperPsiElement implements StringTemplateClassBody {

  public StringTemplateClassBodyImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull StringTemplateVisitor visitor) {
    visitor.visitClassBody(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof StringTemplateVisitor) accept((StringTemplateVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<StringTemplateClassDefinition> getClassDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateClassDefinition.class);
  }

  @Override
  @NotNull
  public List<StringTemplateConditionalIf> getConditionalIfList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateConditionalIf.class);
  }

  @Override
  @NotNull
  public List<StringTemplateEnumEntries> getEnumEntriesList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateEnumEntries.class);
  }

  @Override
  @NotNull
  public List<StringTemplateExplicitConstructorDefinition> getExplicitConstructorDefinitionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateExplicitConstructorDefinition.class);
  }

  @Override
  @NotNull
  public List<StringTemplateFunction> getFunctionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateFunction.class);
  }

  @Override
  @NotNull
  public List<StringTemplateObject> getObjectList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateObject.class);
  }

  @Override
  @NotNull
  public List<StringTemplateVariableDeclaration> getVariableDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, StringTemplateVariableDeclaration.class);
  }

}
