// This is a generated file. Not intended for manual editing.
package com.babestudios.stringtemplate.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class StringTemplateParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return stringTemplateFile(b, l + 1);
  }

  /* ********************************************************** */
  // <<a>> (<<b>> <<a>>)*
  static boolean a_b_a(PsiBuilder b, int l, Parser _a, Parser _b) {
    if (!recursion_guard_(b, l, "a_b_a")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = _a.parse(b, l);
    r = r && a_b_a_1(b, l + 1, _b, _a);
    exit_section_(b, m, null, r);
    return r;
  }

  // (<<b>> <<a>>)*
  private static boolean a_b_a_1(PsiBuilder b, int l, Parser _b, Parser _a) {
    if (!recursion_guard_(b, l, "a_b_a_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!a_b_a_1_0(b, l + 1, _b, _a)) break;
      if (!empty_element_parsed_guard_(b, "a_b_a_1", c)) break;
    }
    return true;
  }

  // <<b>> <<a>>
  private static boolean a_b_a_1_0(PsiBuilder b, int l, Parser _b, Parser _a) {
    if (!recursion_guard_(b, l, "a_b_a_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = _b.parse(b, l);
    r = r && _a.parse(b, l);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<a>> (<<b>> <<a>>)*
  static boolean a_b_a_p(PsiBuilder b, int l, Parser _a, Parser _b) {
    if (!recursion_guard_(b, l, "a_b_a_p")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = _a.parse(b, l);
    p = r; // pin = 1
    r = r && a_b_a_p_1(b, l + 1, _b, _a);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  // (<<b>> <<a>>)*
  private static boolean a_b_a_p_1(PsiBuilder b, int l, Parser _b, Parser _a) {
    if (!recursion_guard_(b, l, "a_b_a_p_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!a_b_a_p_1_0(b, l + 1, _b, _a)) break;
      if (!empty_element_parsed_guard_(b, "a_b_a_p_1", c)) break;
    }
    return true;
  }

  // <<b>> <<a>>
  private static boolean a_b_a_p_1_0(PsiBuilder b, int l, Parser _b, Parser _a) {
    if (!recursion_guard_(b, l, "a_b_a_p_1_0")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_);
    r = _b.parse(b, l);
    p = r; // pin = 1
    r = r && _a.parse(b, l);
    exit_section_(b, l, m, r, p, null);
    return r || p;
  }

  /* ********************************************************** */
  // T_AT IDENTIFIER [argument_list]
  public static boolean annotation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation")) return false;
    if (!nextTokenIs(b, T_AT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, T_AT, IDENTIFIER);
    r = r && annotation_2(b, l + 1);
    exit_section_(b, m, ANNOTATION, r);
    return r;
  }

  // [argument_list]
  private static boolean annotation_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotation_2")) return false;
    argument_list(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // [conditional_if] [annotation] [named_argument] [T_COLONCOLON] (expression | identifier_part+)
  public static boolean argument(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ARGUMENT, "<argument>");
    r = argument_0(b, l + 1);
    r = r && argument_1(b, l + 1);
    r = r && argument_2(b, l + 1);
    r = r && argument_3(b, l + 1);
    r = r && argument_4(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [conditional_if]
  private static boolean argument_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_0")) return false;
    conditional_if(b, l + 1);
    return true;
  }

  // [annotation]
  private static boolean argument_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_1")) return false;
    annotation(b, l + 1);
    return true;
  }

  // [named_argument]
  private static boolean argument_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_2")) return false;
    named_argument(b, l + 1);
    return true;
  }

  // [T_COLONCOLON]
  private static boolean argument_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_3")) return false;
    consumeToken(b, T_COLONCOLON);
    return true;
  }

  // expression | identifier_part+
  private static boolean argument_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1);
    if (!r) r = argument_4_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier_part+
  private static boolean argument_4_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_4_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "argument_4_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // T_LPAREN (non_empty_argument_list | empty_argument_list) T_RPAREN
  public static boolean argument_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_list")) return false;
    if (!nextTokenIs(b, T_LPAREN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LPAREN);
    r = r && argument_list_1(b, l + 1);
    r = r && consumeToken(b, T_RPAREN);
    exit_section_(b, m, ARGUMENT_LIST, r);
    return r;
  }

  // non_empty_argument_list | empty_argument_list
  private static boolean argument_list_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "argument_list_1")) return false;
    boolean r;
    r = non_empty_argument_list(b, l + 1);
    if (!r) r = empty_argument_list(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // KW_BY expression
  public static boolean by_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "by_expression")) return false;
    if (!nextTokenIs(b, KW_BY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_BY);
    r = r && expression(b, l + 1);
    exit_section_(b, m, BY_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  // T_LBRACE class_body_part * T_RBRACE
  public static boolean class_body(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_body")) return false;
    if (!nextTokenIs(b, T_LBRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LBRACE);
    r = r && class_body_1(b, l + 1);
    r = r && consumeToken(b, T_RBRACE);
    exit_section_(b, m, CLASS_BODY, r);
    return r;
  }

  // class_body_part *
  private static boolean class_body_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_body_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!class_body_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "class_body_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // function | class_definition | explicit_constructor_definition | enum_entries | conditional_if | CONDITIONAL | variable_declaration | object
  static boolean class_body_part(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_body_part")) return false;
    boolean r;
    r = function(b, l + 1);
    if (!r) r = class_definition(b, l + 1);
    if (!r) r = explicit_constructor_definition(b, l + 1);
    if (!r) r = enum_entries(b, l + 1);
    if (!r) r = conditional_if(b, l + 1);
    if (!r) r = consumeToken(b, CONDITIONAL);
    if (!r) r = variable_declaration(b, l + 1);
    if (!r) r = object(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // modifier_list class_keyword constructor_definition [class_body]
  public static boolean class_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_definition")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CLASS_DEFINITION, "<class definition>");
    r = modifier_list(b, l + 1);
    r = r && class_keyword(b, l + 1);
    r = r && constructor_definition(b, l + 1);
    r = r && class_definition_3(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [class_body]
  private static boolean class_definition_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_definition_3")) return false;
    class_body(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // KW_CLASS | KW_INTERFACE
  static boolean class_keyword(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_keyword")) return false;
    if (!nextTokenIs(b, "", KW_CLASS, KW_INTERFACE)) return false;
    boolean r;
    r = consumeToken(b, KW_CLASS);
    if (!r) r = consumeToken(b, KW_INTERFACE);
    return r;
  }

  /* ********************************************************** */
  // T_LBRACE [lambda_parameter_list] expression * T_RBRACE
  public static boolean code_block_lambda(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "code_block_lambda")) return false;
    if (!nextTokenIs(b, T_LBRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LBRACE);
    r = r && code_block_lambda_1(b, l + 1);
    r = r && code_block_lambda_2(b, l + 1);
    r = r && consumeToken(b, T_RBRACE);
    exit_section_(b, m, CODE_BLOCK_LAMBDA, r);
    return r;
  }

  // [lambda_parameter_list]
  private static boolean code_block_lambda_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "code_block_lambda_1")) return false;
    lambda_parameter_list(b, l + 1);
    return true;
  }

  // expression *
  private static boolean code_block_lambda_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "code_block_lambda_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!expression(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "code_block_lambda_2", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // <<a_b_a_p <<item>> fast_comma>>
  static boolean comma_list_p(PsiBuilder b, int l, Parser _item) {
    return a_b_a_p(b, l + 1, _item, StringTemplateParser::fast_comma);
  }

  /* ********************************************************** */
  // CONDITIONAL_IF_START MODEL_STATEMENT CONDITIONAL_IF_END
  public static boolean conditional_if(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "conditional_if")) return false;
    if (!nextTokenIs(b, CONDITIONAL_IF_START)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, CONDITIONAL_IF_START, MODEL_STATEMENT, CONDITIONAL_IF_END);
    exit_section_(b, m, CONDITIONAL_IF, r);
    return r;
  }

  /* ********************************************************** */
  // implicit_constructor_definition | (identifier_part+ [annotation] modifier_list explicit_constructor_definition)
  public static boolean constructor_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constructor_definition")) return false;
    if (!nextTokenIs(b, "<constructor definition>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONSTRUCTOR_DEFINITION, "<constructor definition>");
    r = implicit_constructor_definition(b, l + 1);
    if (!r) r = constructor_definition_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+ [annotation] modifier_list explicit_constructor_definition
  private static boolean constructor_definition_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constructor_definition_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = constructor_definition_1_0(b, l + 1);
    r = r && constructor_definition_1_1(b, l + 1);
    r = r && modifier_list(b, l + 1);
    r = r && explicit_constructor_definition(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier_part+
  private static boolean constructor_definition_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constructor_definition_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "constructor_definition_1_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [annotation]
  private static boolean constructor_definition_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constructor_definition_1_1")) return false;
    annotation(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // variable_declaration | typealias_declaration
  public static boolean declaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "declaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, DECLARATION, "<declaration>");
    r = variable_declaration(b, l + 1);
    if (!r) r = typealias_declaration(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'const'
  //                    | 'lateinit'
  //                    | 'override'
  //                    | 'private'
  //                    | 'static'
  //                    | 'synchronized' !'('
  static boolean declaration_modifier(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "declaration_modifier")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_CONST);
    if (!r) r = consumeToken(b, KW_LATEINIT);
    if (!r) r = consumeToken(b, KW_OVERRIDE);
    if (!r) r = consumeToken(b, KW_PRIVATE);
    if (!r) r = consumeToken(b, KW_STATIC);
    if (!r) r = declaration_modifier_5(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // 'synchronized' !'('
  private static boolean declaration_modifier_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "declaration_modifier_5")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_SYNCHRONIZED);
    r = r && declaration_modifier_5_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // !'('
  private static boolean declaration_modifier_5_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "declaration_modifier_5_1")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !consumeToken(b, T_LPAREN);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // model_definition | DOT_QUALIFIED_EXPRESSION_PART_LITERAL
  static boolean dot_qualified_expression_part(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "dot_qualified_expression_part")) return false;
    if (!nextTokenIs(b, "", DOT_QUALIFIED_EXPRESSION_PART_LITERAL, MODEL_STATEMENT)) return false;
    boolean r;
    r = model_definition(b, l + 1);
    if (!r) r = consumeToken(b, DOT_QUALIFIED_EXPRESSION_PART_LITERAL);
    return r;
  }

  /* ********************************************************** */
  // T_ELVIS expression
  public static boolean elvis_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "elvis_expression")) return false;
    if (!nextTokenIs(b, T_ELVIS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_ELVIS);
    r = r && expression(b, l + 1);
    exit_section_(b, m, ELVIS_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  public static boolean empty_argument_list(PsiBuilder b, int l) {
    Marker m = enter_section_(b);
    exit_section_(b, m, EMPTY_ARGUMENT_LIST, true);
    return true;
  }

  /* ********************************************************** */
  public static boolean empty_modifier_list(PsiBuilder b, int l) {
    Marker m = enter_section_(b);
    exit_section_(b, m, EMPTY_MODIFIER_LIST, true);
    return true;
  }

  /* ********************************************************** */
  public static boolean empty_parameter_list(PsiBuilder b, int l) {
    Marker m = enter_section_(b);
    exit_section_(b, m, EMPTY_PARAMETER_LIST, true);
    return true;
  }

  /* ********************************************************** */
  public static boolean empty_type_parameter_list(PsiBuilder b, int l) {
    Marker m = enter_section_(b);
    exit_section_(b, m, EMPTY_TYPE_PARAMETER_LIST, true);
    return true;
  }

  /* ********************************************************** */
  // <<comma_list_p (enum_entry mb_nl)>> T_SEMICOLON
  public static boolean enum_entries(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entries")) return false;
    if (!nextTokenIs(b, "<enum entries>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ENUM_ENTRIES, "<enum entries>");
    r = comma_list_p(b, l + 1, StringTemplateParser::enum_entries_0_0);
    r = r && consumeToken(b, T_SEMICOLON);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // enum_entry mb_nl
  private static boolean enum_entries_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entries_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = enum_entry(b, l + 1);
    r = r && mb_nl(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // identifier_part+ [T_LPAREN] [STRING_LITERAL] [T_RPAREN]
  public static boolean enum_entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entry")) return false;
    if (!nextTokenIs(b, "<enum entry>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ENUM_ENTRY, "<enum entry>");
    r = enum_entry_0(b, l + 1);
    r = r && enum_entry_1(b, l + 1);
    r = r && enum_entry_2(b, l + 1);
    r = r && enum_entry_3(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+
  private static boolean enum_entry_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entry_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "enum_entry_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [T_LPAREN]
  private static boolean enum_entry_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entry_1")) return false;
    consumeToken(b, T_LPAREN);
    return true;
  }

  // [STRING_LITERAL]
  private static boolean enum_entry_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entry_2")) return false;
    consumeToken(b, STRING_LITERAL);
    return true;
  }

  // [T_RPAREN]
  private static boolean enum_entry_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "enum_entry_3")) return false;
    consumeToken(b, T_RPAREN);
    return true;
  }

  /* ********************************************************** */
  // T_EQ expression
  public static boolean eq_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eq_expression")) return false;
    if (!nextTokenIs(b, T_EQ)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_EQ);
    r = r && expression(b, l + 1);
    exit_section_(b, m, EQ_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  // KW_CONSTRUCTOR [parameter_list] [super_type_list]
  public static boolean explicit_constructor_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "explicit_constructor_definition")) return false;
    if (!nextTokenIs(b, KW_CONSTRUCTOR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_CONSTRUCTOR);
    r = r && explicit_constructor_definition_1(b, l + 1);
    r = r && explicit_constructor_definition_2(b, l + 1);
    exit_section_(b, m, EXPLICIT_CONSTRUCTOR_DEFINITION, r);
    return r;
  }

  // [parameter_list]
  private static boolean explicit_constructor_definition_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "explicit_constructor_definition_1")) return false;
    parameter_list(b, l + 1);
    return true;
  }

  // [super_type_list]
  private static boolean explicit_constructor_definition_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "explicit_constructor_definition_2")) return false;
    super_type_list(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // [KW_RETURN] ( STRING_LITERAL | INTEGER_CONSTANT | LONG_CONSTANT | FLOAT_CONSTANT | HEXA_CONSTANT | function_call | code_block_lambda | variable_reference | variable_declaration | when_expression | object)
  public static boolean expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EXPRESSION, "<expression>");
    r = expression_0(b, l + 1);
    r = r && expression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [KW_RETURN]
  private static boolean expression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expression_0")) return false;
    consumeToken(b, KW_RETURN);
    return true;
  }

  // STRING_LITERAL | INTEGER_CONSTANT | LONG_CONSTANT | FLOAT_CONSTANT | HEXA_CONSTANT | function_call | code_block_lambda | variable_reference | variable_declaration | when_expression | object
  private static boolean expression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expression_1")) return false;
    boolean r;
    r = consumeToken(b, STRING_LITERAL);
    if (!r) r = consumeToken(b, INTEGER_CONSTANT);
    if (!r) r = consumeToken(b, LONG_CONSTANT);
    if (!r) r = consumeToken(b, FLOAT_CONSTANT);
    if (!r) r = consumeToken(b, HEXA_CONSTANT);
    if (!r) r = function_call(b, l + 1);
    if (!r) r = code_block_lambda(b, l + 1);
    if (!r) r = variable_reference(b, l + 1);
    if (!r) r = variable_declaration(b, l + 1);
    if (!r) r = when_expression(b, l + 1);
    if (!r) r = object(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // ',' [COMMENT] [CONDITIONAL]
  static boolean fast_comma(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fast_comma")) return false;
    if (!nextTokenIs(b, T_COMMA)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_COMMA);
    r = r && fast_comma_1(b, l + 1);
    r = r && fast_comma_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // [COMMENT]
  private static boolean fast_comma_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fast_comma_1")) return false;
    consumeToken(b, COMMENT);
    return true;
  }

  // [CONDITIONAL]
  private static boolean fast_comma_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fast_comma_2")) return false;
    consumeToken(b, CONDITIONAL);
    return true;
  }

  /* ********************************************************** */
  // modifier_list 'fun' identifier_part + parameter_list [return_type] [function_body]
  public static boolean function(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION, "<function>");
    r = modifier_list(b, l + 1);
    r = r && consumeToken(b, KW_FUN);
    r = r && function_2(b, l + 1);
    r = r && parameter_list(b, l + 1);
    r = r && function_4(b, l + 1);
    r = r && function_5(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part +
  private static boolean function_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "function_2", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [return_type]
  private static boolean function_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_4")) return false;
    return_type(b, l + 1);
    return true;
  }

  // [function_body]
  private static boolean function_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_5")) return false;
    function_body(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // function_body_eq | function_body_braces
  public static boolean function_body(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_body")) return false;
    if (!nextTokenIs(b, "<function body>", T_EQ, T_LBRACE)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_BODY, "<function body>");
    r = function_body_eq(b, l + 1);
    if (!r) r = function_body_braces(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // T_LBRACE function_body_part* T_RBRACE
  static boolean function_body_braces(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_body_braces")) return false;
    if (!nextTokenIs(b, T_LBRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LBRACE);
    r = r && function_body_braces_1(b, l + 1);
    r = r && consumeToken(b, T_RBRACE);
    exit_section_(b, m, null, r);
    return r;
  }

  // function_body_part*
  private static boolean function_body_braces_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_body_braces_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!function_body_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "function_body_braces_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // T_EQ expression
  static boolean function_body_eq(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_body_eq")) return false;
    if (!nextTokenIs(b, T_EQ)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_EQ);
    r = r && expression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // expression | identifier_part | conditional_if | CONDITIONAL | variable_declaration | function_call
  static boolean function_body_part(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_body_part")) return false;
    boolean r;
    r = expression(b, l + 1);
    if (!r) r = identifier_part(b, l + 1);
    if (!r) r = conditional_if(b, l + 1);
    if (!r) r = consumeToken(b, CONDITIONAL);
    if (!r) r = variable_declaration(b, l + 1);
    if (!r) r = function_call(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // identifier_part+ argument_list temp_1 * [code_block_lambda | elvis_expression]
  public static boolean function_call(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call")) return false;
    if (!nextTokenIs(b, "<function call>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_CALL, "<function call>");
    r = function_call_0(b, l + 1);
    r = r && argument_list(b, l + 1);
    r = r && function_call_2(b, l + 1);
    r = r && function_call_3(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+
  private static boolean function_call_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "function_call_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // temp_1 *
  private static boolean function_call_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!temp_1(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "function_call_2", c)) break;
    }
    return true;
  }

  // [code_block_lambda | elvis_expression]
  private static boolean function_call_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_3")) return false;
    function_call_3_0(b, l + 1);
    return true;
  }

  // code_block_lambda | elvis_expression
  private static boolean function_call_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_call_3_0")) return false;
    boolean r;
    r = code_block_lambda(b, l + 1);
    if (!r) r = elvis_expression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // T_LPAREN type_parameter_list T_RPAREN T_ARROW type_reference
  public static boolean function_reference(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "function_reference")) return false;
    if (!nextTokenIs(b, T_LPAREN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LPAREN);
    r = r && type_parameter_list(b, l + 1);
    r = r && consumeTokens(b, 0, T_RPAREN, T_ARROW);
    r = r && type_reference(b, l + 1);
    exit_section_(b, m, FUNCTION_REFERENCE, r);
    return r;
  }

  /* ********************************************************** */
  // T_LT non_empty_type_parameter_list T_GT
  public static boolean generic_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "generic_type")) return false;
    if (!nextTokenIs(b, T_LT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LT);
    r = r && non_empty_type_parameter_list(b, l + 1);
    r = r && consumeToken(b, T_GT);
    exit_section_(b, m, GENERIC_TYPE, r);
    return r;
  }

  /* ********************************************************** */
  // model_definition | IDENTIFIER
  static boolean identifier_part(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifier_part")) return false;
    if (!nextTokenIs(b, "", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    r = model_definition(b, l + 1);
    if (!r) r = consumeToken(b, IDENTIFIER);
    return r;
  }

  /* ********************************************************** */
  // identifier_part+ [parameter_list] [super_type_list]
  public static boolean implicit_constructor_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implicit_constructor_definition")) return false;
    if (!nextTokenIs(b, "<implicit constructor definition>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, IMPLICIT_CONSTRUCTOR_DEFINITION, "<implicit constructor definition>");
    r = implicit_constructor_definition_0(b, l + 1);
    r = r && implicit_constructor_definition_1(b, l + 1);
    r = r && implicit_constructor_definition_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+
  private static boolean implicit_constructor_definition_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implicit_constructor_definition_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "implicit_constructor_definition_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [parameter_list]
  private static boolean implicit_constructor_definition_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implicit_constructor_definition_1")) return false;
    parameter_list(b, l + 1);
    return true;
  }

  // [super_type_list]
  private static boolean implicit_constructor_definition_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "implicit_constructor_definition_2")) return false;
    super_type_list(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'import' (dot_qualified_expression_part)+
  public static boolean import_directive(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "import_directive")) return false;
    if (!nextTokenIs(b, KW_IMPORT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_IMPORT);
    r = r && import_directive_1(b, l + 1);
    exit_section_(b, m, IMPORT_DIRECTIVE, r);
    return r;
  }

  // (dot_qualified_expression_part)+
  private static boolean import_directive_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "import_directive_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = import_directive_1_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!import_directive_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "import_directive_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // (dot_qualified_expression_part)
  private static boolean import_directive_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "import_directive_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = dot_qualified_expression_part(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // non_empty_argument_list T_ARROW
  public static boolean lambda_parameter_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambda_parameter_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LAMBDA_PARAMETER_LIST, "<lambda parameter list>");
    r = non_empty_argument_list(b, l + 1);
    r = r && consumeToken(b, T_ARROW);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // nl*
  static boolean mb_nl(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "mb_nl")) return false;
    while (true) {
      int c = current_position_(b);
      if (!nl(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "mb_nl", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // MODEL_STATEMENT [MODEL_FORMATTER] MODEL_CLOSING
  public static boolean model_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "model_definition")) return false;
    if (!nextTokenIs(b, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, MODEL_STATEMENT);
    r = r && model_definition_1(b, l + 1);
    r = r && consumeToken(b, MODEL_CLOSING);
    exit_section_(b, m, MODEL_DEFINITION, r);
    return r;
  }

  // [MODEL_FORMATTER]
  private static boolean model_definition_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "model_definition_1")) return false;
    consumeToken(b, MODEL_FORMATTER);
    return true;
  }

  /* ********************************************************** */
  // KW_ABSTRACT
  //                    | KW_OPEN
  //                    | KW_DATA
  //                    | KW_ENUM
  //                    | KW_INTERNAL
  //                    | KW_OVERRIDE
  //                    | KW_PRIVATE
  //                    | KW_SUSPEND
  //                    | KW_STATIC
  //                    | KW_SYNCHRONIZED !'('
  //                    | KW_TRANSIENT
  //                    | KW_SEALED
  static boolean modifier(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modifier")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, KW_ABSTRACT);
    if (!r) r = consumeTokenFast(b, KW_OPEN);
    if (!r) r = consumeTokenFast(b, KW_DATA);
    if (!r) r = consumeTokenFast(b, KW_ENUM);
    if (!r) r = consumeTokenFast(b, KW_INTERNAL);
    if (!r) r = consumeTokenFast(b, KW_OVERRIDE);
    if (!r) r = consumeTokenFast(b, KW_PRIVATE);
    if (!r) r = consumeTokenFast(b, KW_SUSPEND);
    if (!r) r = consumeTokenFast(b, KW_STATIC);
    if (!r) r = modifier_9(b, l + 1);
    if (!r) r = consumeTokenFast(b, KW_TRANSIENT);
    if (!r) r = consumeTokenFast(b, KW_SEALED);
    exit_section_(b, m, null, r);
    return r;
  }

  // KW_SYNCHRONIZED !'('
  private static boolean modifier_9(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modifier_9")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, KW_SYNCHRONIZED);
    r = r && modifier_9_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // !'('
  private static boolean modifier_9_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modifier_9_1")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !consumeTokenFast(b, T_LPAREN);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // non_empty_modifier_list | empty_modifier_list
  public static boolean modifier_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modifier_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, MODIFIER_LIST, "<modifier list>");
    r = non_empty_modifier_list(b, l + 1);
    if (!r) r = empty_modifier_list(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // identifier_part+ T_EQ
  public static boolean named_argument(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "named_argument")) return false;
    if (!nextTokenIs(b, "<named argument>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, NAMED_ARGUMENT, "<named argument>");
    r = named_argument_0(b, l + 1);
    r = r && consumeToken(b, T_EQ);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+
  private static boolean named_argument_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "named_argument_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "named_argument_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // NL
  static boolean nl(PsiBuilder b, int l) {
    return consumeTokenFast(b, NL);
  }

  /* ********************************************************** */
  // <<comma_list_p (argument mb_nl)>>
  public static boolean non_empty_argument_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_argument_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, NON_EMPTY_ARGUMENT_LIST, "<non empty argument list>");
    r = comma_list_p(b, l + 1, StringTemplateParser::non_empty_argument_list_0_0);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // argument mb_nl
  private static boolean non_empty_argument_list_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_argument_list_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = argument(b, l + 1);
    r = r && mb_nl(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<a_b_a (modifier | annotation) mb_nl>>
  public static boolean non_empty_modifier_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_modifier_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, NON_EMPTY_MODIFIER_LIST, "<non empty modifier list>");
    r = a_b_a(b, l + 1, StringTemplateParser::non_empty_modifier_list_0_0, StringTemplateParser::mb_nl);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // modifier | annotation
  private static boolean non_empty_modifier_list_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_modifier_list_0_0")) return false;
    boolean r;
    r = modifier(b, l + 1);
    if (!r) r = annotation(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // <<comma_list_p (parameter mb_nl)>>
  public static boolean non_empty_parameter_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_parameter_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, NON_EMPTY_PARAMETER_LIST, "<non empty parameter list>");
    r = comma_list_p(b, l + 1, StringTemplateParser::non_empty_parameter_list_0_0);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // parameter mb_nl
  private static boolean non_empty_parameter_list_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_parameter_list_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = parameter(b, l + 1);
    r = r && mb_nl(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<comma_list_p (type_parameter mb_nl)>>
  public static boolean non_empty_type_parameter_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_type_parameter_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, NON_EMPTY_TYPE_PARAMETER_LIST, "<non empty type parameter list>");
    r = comma_list_p(b, l + 1, StringTemplateParser::non_empty_type_parameter_list_0_0);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // type_parameter mb_nl
  private static boolean non_empty_type_parameter_list_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "non_empty_type_parameter_list_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = type_parameter(b, l + 1);
    r = r && mb_nl(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // modifier_list [KW_COMPANION] KW_OBJECT identifier_part * T_COLON type_parameter_list identifier_part * [argument_list] [class_body]
  public static boolean object(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, OBJECT, "<object>");
    r = modifier_list(b, l + 1);
    r = r && object_1(b, l + 1);
    r = r && consumeToken(b, KW_OBJECT);
    r = r && object_3(b, l + 1);
    r = r && consumeToken(b, T_COLON);
    r = r && type_parameter_list(b, l + 1);
    r = r && object_6(b, l + 1);
    r = r && object_7(b, l + 1);
    r = r && object_8(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [KW_COMPANION]
  private static boolean object_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object_1")) return false;
    consumeToken(b, KW_COMPANION);
    return true;
  }

  // identifier_part *
  private static boolean object_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "object_3", c)) break;
    }
    return true;
  }

  // identifier_part *
  private static boolean object_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object_6")) return false;
    while (true) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "object_6", c)) break;
    }
    return true;
  }

  // [argument_list]
  private static boolean object_7(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object_7")) return false;
    argument_list(b, l + 1);
    return true;
  }

  // [class_body]
  private static boolean object_8(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "object_8")) return false;
    class_body(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'package' (dot_qualified_expression_part)+
  public static boolean package_directive(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "package_directive")) return false;
    if (!nextTokenIs(b, KW_PACKAGE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_PACKAGE);
    r = r && package_directive_1(b, l + 1);
    exit_section_(b, m, PACKAGE_DIRECTIVE, r);
    return r;
  }

  // (dot_qualified_expression_part)+
  private static boolean package_directive_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "package_directive_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = package_directive_1_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!package_directive_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "package_directive_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // (dot_qualified_expression_part)
  private static boolean package_directive_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "package_directive_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = dot_qualified_expression_part(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // [conditional_if] [annotation] modifier_list [KW_VAL] identifier_part+ T_COLON [annotation] type_reference [CONDITIONAL]
  public static boolean parameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARAMETER, "<parameter>");
    r = parameter_0(b, l + 1);
    r = r && parameter_1(b, l + 1);
    r = r && modifier_list(b, l + 1);
    r = r && parameter_3(b, l + 1);
    r = r && parameter_4(b, l + 1);
    r = r && consumeToken(b, T_COLON);
    r = r && parameter_6(b, l + 1);
    r = r && type_reference(b, l + 1);
    r = r && parameter_8(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [conditional_if]
  private static boolean parameter_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_0")) return false;
    conditional_if(b, l + 1);
    return true;
  }

  // [annotation]
  private static boolean parameter_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_1")) return false;
    annotation(b, l + 1);
    return true;
  }

  // [KW_VAL]
  private static boolean parameter_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_3")) return false;
    consumeToken(b, KW_VAL);
    return true;
  }

  // identifier_part+
  private static boolean parameter_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "parameter_4", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [annotation]
  private static boolean parameter_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_6")) return false;
    annotation(b, l + 1);
    return true;
  }

  // [CONDITIONAL]
  private static boolean parameter_8(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_8")) return false;
    consumeToken(b, CONDITIONAL);
    return true;
  }

  /* ********************************************************** */
  // T_LPAREN (non_empty_parameter_list | empty_parameter_list) T_RPAREN
  public static boolean parameter_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_list")) return false;
    if (!nextTokenIs(b, T_LPAREN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LPAREN);
    r = r && parameter_list_1(b, l + 1);
    r = r && consumeToken(b, T_RPAREN);
    exit_section_(b, m, PARAMETER_LIST, r);
    return r;
  }

  // non_empty_parameter_list | empty_parameter_list
  private static boolean parameter_list_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameter_list_1")) return false;
    boolean r;
    r = non_empty_parameter_list(b, l + 1);
    if (!r) r = empty_parameter_list(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // MODEL_STATEMENT | KEY | VALUE | SEPARATOR | MODEL_FORMATTER
  public static boolean property(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PROPERTY, "<property>");
    r = consumeToken(b, MODEL_STATEMENT);
    if (!r) r = consumeToken(b, KEY);
    if (!r) r = consumeToken(b, VALUE);
    if (!r) r = consumeToken(b, SEPARATOR);
    if (!r) r = consumeToken(b, MODEL_FORMATTER);
    exit_section_(b, l, m, r, false, StringTemplateParser::recover_property);
    return r;
  }

  /* ********************************************************** */
  // KW_GET T_LPAREN T_RPAREN eq_expression
  public static boolean property_accessor(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_accessor")) return false;
    if (!nextTokenIs(b, KW_GET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, KW_GET, T_LPAREN, T_RPAREN);
    r = r && eq_expression(b, l + 1);
    exit_section_(b, m, PROPERTY_ACCESSOR, r);
    return r;
  }

  /* ********************************************************** */
  // !(KEY|SEPARATOR)
  static boolean recover_property(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "recover_property")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !recover_property_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // KEY|SEPARATOR
  private static boolean recover_property_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "recover_property_0")) return false;
    boolean r;
    r = consumeToken(b, KEY);
    if (!r) r = consumeToken(b, SEPARATOR);
    return r;
  }

  /* ********************************************************** */
  // T_COLON type_definition
  public static boolean return_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "return_type")) return false;
    if (!nextTokenIs(b, T_COLON)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_COLON);
    r = r && type_definition(b, l + 1);
    exit_section_(b, m, RETURN_TYPE, r);
    return r;
  }

  /* ********************************************************** */
  // (package_directive|import_directive|declaration|class_definition|function|CRLF|conditional_if|CONDITIONAL)+
  public static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ROOT, "<root>");
    r = root_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!root_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "root", c)) break;
    }
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // package_directive|import_directive|declaration|class_definition|function|CRLF|conditional_if|CONDITIONAL
  private static boolean root_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_0")) return false;
    boolean r;
    r = package_directive(b, l + 1);
    if (!r) r = import_directive(b, l + 1);
    if (!r) r = declaration(b, l + 1);
    if (!r) r = class_definition(b, l + 1);
    if (!r) r = function(b, l + 1);
    if (!r) r = consumeToken(b, CRLF);
    if (!r) r = conditional_if(b, l + 1);
    if (!r) r = consumeToken(b, CONDITIONAL);
    return r;
  }

  /* ********************************************************** */
  // root
  static boolean stringTemplateFile(PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // T_COLON (function_call | non_empty_type_parameter_list) [argument_list]
  public static boolean super_type_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "super_type_list")) return false;
    if (!nextTokenIs(b, T_COLON)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_COLON);
    r = r && super_type_list_1(b, l + 1);
    r = r && super_type_list_2(b, l + 1);
    exit_section_(b, m, SUPER_TYPE_LIST, r);
    return r;
  }

  // function_call | non_empty_type_parameter_list
  private static boolean super_type_list_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "super_type_list_1")) return false;
    boolean r;
    r = function_call(b, l + 1);
    if (!r) r = non_empty_type_parameter_list(b, l + 1);
    return r;
  }

  // [argument_list]
  private static boolean super_type_list_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "super_type_list_2")) return false;
    argument_list(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // conditional_if | CONDITIONAL | function_call | identifier_part+ | expression
  static boolean temp_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "temp_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = conditional_if(b, l + 1);
    if (!r) r = consumeToken(b, CONDITIONAL);
    if (!r) r = function_call(b, l + 1);
    if (!r) r = temp_1_3(b, l + 1);
    if (!r) r = expression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier_part+
  private static boolean temp_1_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "temp_1_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "temp_1_3", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (identifier_part | KW_BY) + [generic_type]
  public static boolean type_definition(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_definition")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TYPE_DEFINITION, "<type definition>");
    r = type_definition_0(b, l + 1);
    r = r && type_definition_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (identifier_part | KW_BY) +
  private static boolean type_definition_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_definition_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = type_definition_0_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!type_definition_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "type_definition_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier_part | KW_BY
  private static boolean type_definition_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_definition_0_0")) return false;
    boolean r;
    r = identifier_part(b, l + 1);
    if (!r) r = consumeToken(b, KW_BY);
    return r;
  }

  // [generic_type]
  private static boolean type_definition_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_definition_1")) return false;
    generic_type(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // [annotation] type_reference [CONDITIONAL]
  public static boolean type_parameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_parameter")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TYPE_PARAMETER, "<type parameter>");
    r = type_parameter_0(b, l + 1);
    r = r && type_reference(b, l + 1);
    r = r && type_parameter_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [annotation]
  private static boolean type_parameter_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_parameter_0")) return false;
    annotation(b, l + 1);
    return true;
  }

  // [CONDITIONAL]
  private static boolean type_parameter_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_parameter_2")) return false;
    consumeToken(b, CONDITIONAL);
    return true;
  }

  /* ********************************************************** */
  // non_empty_type_parameter_list | empty_type_parameter_list
  public static boolean type_parameter_list(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_parameter_list")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TYPE_PARAMETER_LIST, "<type parameter list>");
    r = non_empty_type_parameter_list(b, l + 1);
    if (!r) r = empty_type_parameter_list(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // function_reference | type_definition
  public static boolean type_reference(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_reference")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TYPE_REFERENCE, "<type reference>");
    r = function_reference(b, l + 1);
    if (!r) r = type_definition(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // KW_TYPE_ALIAS identifier_part + T_EQ type_reference
  public static boolean typealias_declaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typealias_declaration")) return false;
    if (!nextTokenIs(b, KW_TYPE_ALIAS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, KW_TYPE_ALIAS);
    r = r && typealias_declaration_1(b, l + 1);
    r = r && consumeToken(b, T_EQ);
    r = r && type_reference(b, l + 1);
    exit_section_(b, m, TYPEALIAS_DECLARATION, r);
    return r;
  }

  // identifier_part +
  private static boolean typealias_declaration_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typealias_declaration_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "typealias_declaration_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // [declaration_modifier] (KW_VAL | KW_VAR) identifier_part + [T_COLON type_reference] [property_accessor | eq_expression | by_expression]
  public static boolean variable_declaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VARIABLE_DECLARATION, "<variable declaration>");
    r = variable_declaration_0(b, l + 1);
    r = r && variable_declaration_1(b, l + 1);
    r = r && variable_declaration_2(b, l + 1);
    r = r && variable_declaration_3(b, l + 1);
    r = r && variable_declaration_4(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [declaration_modifier]
  private static boolean variable_declaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_0")) return false;
    declaration_modifier(b, l + 1);
    return true;
  }

  // KW_VAL | KW_VAR
  private static boolean variable_declaration_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_1")) return false;
    boolean r;
    r = consumeToken(b, KW_VAL);
    if (!r) r = consumeToken(b, KW_VAR);
    return r;
  }

  // identifier_part +
  private static boolean variable_declaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "variable_declaration_2", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [T_COLON type_reference]
  private static boolean variable_declaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_3")) return false;
    variable_declaration_3_0(b, l + 1);
    return true;
  }

  // T_COLON type_reference
  private static boolean variable_declaration_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_COLON);
    r = r && type_reference(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // [property_accessor | eq_expression | by_expression]
  private static boolean variable_declaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_4")) return false;
    variable_declaration_4_0(b, l + 1);
    return true;
  }

  // property_accessor | eq_expression | by_expression
  private static boolean variable_declaration_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_declaration_4_0")) return false;
    boolean r;
    r = property_accessor(b, l + 1);
    if (!r) r = eq_expression(b, l + 1);
    if (!r) r = by_expression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // identifier_part+ [T_ELVIS (expression | identifier_part+)]
  public static boolean variable_reference(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference")) return false;
    if (!nextTokenIs(b, "<variable reference>", IDENTIFIER, MODEL_STATEMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VARIABLE_REFERENCE, "<variable reference>");
    r = variable_reference_0(b, l + 1);
    r = r && variable_reference_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // identifier_part+
  private static boolean variable_reference_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "variable_reference_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // [T_ELVIS (expression | identifier_part+)]
  private static boolean variable_reference_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference_1")) return false;
    variable_reference_1_0(b, l + 1);
    return true;
  }

  // T_ELVIS (expression | identifier_part+)
  private static boolean variable_reference_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_ELVIS);
    r = r && variable_reference_1_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // expression | identifier_part+
  private static boolean variable_reference_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference_1_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1);
    if (!r) r = variable_reference_1_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // identifier_part+
  private static boolean variable_reference_1_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_reference_1_0_1_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifier_part(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!identifier_part(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "variable_reference_1_0_1_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // KW_WHEN T_LPAREN [KW_VAL] argument T_RPAREN (T_LBRACE when_expression_body + T_RBRACE)
  public static boolean when_expression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression")) return false;
    if (!nextTokenIs(b, KW_WHEN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, KW_WHEN, T_LPAREN);
    r = r && when_expression_2(b, l + 1);
    r = r && argument(b, l + 1);
    r = r && consumeToken(b, T_RPAREN);
    r = r && when_expression_5(b, l + 1);
    exit_section_(b, m, WHEN_EXPRESSION, r);
    return r;
  }

  // [KW_VAL]
  private static boolean when_expression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_2")) return false;
    consumeToken(b, KW_VAL);
    return true;
  }

  // T_LBRACE when_expression_body + T_RBRACE
  private static boolean when_expression_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_5")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, T_LBRACE);
    r = r && when_expression_5_1(b, l + 1);
    r = r && consumeToken(b, T_RBRACE);
    exit_section_(b, m, null, r);
    return r;
  }

  // when_expression_body +
  private static boolean when_expression_5_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_5_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = when_expression_body(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!when_expression_body(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "when_expression_5_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // [KW_IS] temp_1 + T_ARROW temp_1 +
  public static boolean when_expression_body(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_body")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, WHEN_EXPRESSION_BODY, "<when expression body>");
    r = when_expression_body_0(b, l + 1);
    r = r && when_expression_body_1(b, l + 1);
    r = r && consumeToken(b, T_ARROW);
    r = r && when_expression_body_3(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // [KW_IS]
  private static boolean when_expression_body_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_body_0")) return false;
    consumeToken(b, KW_IS);
    return true;
  }

  // temp_1 +
  private static boolean when_expression_body_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_body_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = temp_1(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!temp_1(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "when_expression_body_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // temp_1 +
  private static boolean when_expression_body_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "when_expression_body_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = temp_1(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!temp_1(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "when_expression_body_3", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

}
