package com.babestudios.stringtemplate;

import com.intellij.ide.projectView.PresentationData;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.NavigatablePsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import com.babestudios.stringtemplate.psi.*;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import com.babestudios.stringtemplate.psi.impl.StringTemplatePropertyImpl;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class StringTemplateStructureViewElement implements StructureViewTreeElement, SortableTreeElement {
  private NavigatablePsiElement myElement;

  public StringTemplateStructureViewElement(NavigatablePsiElement element) {
    this.myElement = element;
  }

  @Override
  public Object getValue() {
    return myElement;
  }

  @Override
  public void navigate(boolean requestFocus) {
    myElement.navigate(requestFocus);
  }

  @Override
  public boolean canNavigate() {
    return myElement.canNavigate();
  }

  @Override
  public boolean canNavigateToSource() {
    return myElement.canNavigateToSource();
  }

  @NotNull
  @Override
  public String getAlphaSortKey() {
    String name = myElement.getName();
    return name != null ? name : "";
  }

  @NotNull
  @Override
  public ItemPresentation getPresentation() {
    ItemPresentation presentation = myElement.getPresentation();
    return presentation != null ? presentation : new PresentationData();
  }

  @Override
  public TreeElement[] getChildren() {
    if (myElement instanceof StringTemplateFile) {
      StringTemplateProperty[] properties = PsiTreeUtil.getChildrenOfType(myElement, StringTemplateProperty.class);
      List<TreeElement> treeElements = new ArrayList<TreeElement>(properties.length);
      for (StringTemplateProperty property : properties) {
        treeElements.add(new StringTemplateStructureViewElement((StringTemplatePropertyImpl) property));
      }
      return treeElements.toArray(new TreeElement[treeElements.size()]);
    }
    return EMPTY_ARRAY;
  }
}