package com.babestudios.stringtemplate.format

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.babestudios.stringtemplate.psi.StringTemplateTypes
import com.intellij.formatting.Alignment
import com.intellij.formatting.FormattingContext
import com.intellij.formatting.FormattingModel
import com.intellij.formatting.FormattingModelBuilder
import com.intellij.formatting.FormattingModelProvider
import com.intellij.formatting.Indent
import com.intellij.formatting.SpacingBuilder
import com.intellij.formatting.Wrap
import com.intellij.formatting.WrapType
import com.intellij.psi.codeStyle.CodeStyleSettings

class StringTemplateFormattingModelBuilder : FormattingModelBuilder {

	override fun createModel(formattingContext: FormattingContext): FormattingModel {
		val codeStyleSettings = formattingContext.codeStyleSettings
		return FormattingModelProvider.createFormattingModelForPsiFile(
			formattingContext.containingFile,
			StringTemplateBlock(
				formattingContext.node,
				Wrap.createWrap(WrapType.NONE, false),
				Alignment.createAlignment(),
				Indent.getNoneIndent(),
				createSpaceBuilder(codeStyleSettings)
			),
			codeStyleSettings
		)
	}

	private fun createSpaceBuilder(settings: CodeStyleSettings): SpacingBuilder {
		return SpacingBuilder(settings, StringTemplateLanguage)
			.around(StringTemplateTypes.SEPARATOR)
			.spaceIf(settings.getCommonSettings(StringTemplateLanguage.id).SPACE_AROUND_ASSIGNMENT_OPERATORS)
			.before(StringTemplateTypes.PROPERTY)
			.none()
	}

}
