package com.babestudios.stringtemplate.format

import com.babestudios.stringtemplate.psi.StringTemplateTypes
import com.babestudios.stringtemplate.psi.StringTemplateTypes.ARGUMENT
import com.babestudios.stringtemplate.psi.StringTemplateTypes.CLASS_BODY
import com.babestudios.stringtemplate.psi.StringTemplateTypes.CLASS_DEFINITION
import com.babestudios.stringtemplate.psi.StringTemplateTypes.EXPRESSION
import com.babestudios.stringtemplate.psi.StringTemplateTypes.FUNCTION
import com.babestudios.stringtemplate.psi.StringTemplateTypes.FUNCTION_CALL
import com.babestudios.stringtemplate.psi.StringTemplateTypes.PARAMETER
import com.babestudios.stringtemplate.psi.StringTemplateTypes.WHEN_EXPRESSION_BODY
import com.intellij.formatting.Indent
import com.intellij.formatting.SpacingBuilder
import com.intellij.lang.ASTNode

object Generate {

	fun block(node: ASTNode, spacingBuilder: SpacingBuilder): StringTemplateBlock {

		val type = node.elementType
		val parentType = node.treeParent.elementType

		return when (type) {
			FUNCTION -> StringTemplateBlock(
				node,
				null,
				null,
				if (parentType == FUNCTION || parentType == CLASS_BODY) Indent.getNormalIndent()
				else Indent.getNoneIndent(),
				spacingBuilder
			)

			PARAMETER, ARGUMENT, EXPRESSION, FUNCTION_CALL, WHEN_EXPRESSION_BODY -> StringTemplateBlock(
				node,
				null,
				null,
				Indent.getNormalIndent(),
				spacingBuilder
			)

			CLASS_DEFINITION, StringTemplateTypes.OBJECT -> StringTemplateBlock(
				node,
				null,
				null,
				if (parentType == CLASS_BODY) Indent.getNormalIndent()
				else Indent.getNoneIndent(),
				spacingBuilder
			)

			else -> StringTemplateBlock(node, null, null, Indent.getNoneIndent(), spacingBuilder)
		}
	}

}