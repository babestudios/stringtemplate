package com.babestudios.stringtemplate;

import com.intellij.codeInsight.daemon.*;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.tree.java.PsiJavaTokenImpl;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class StringTemplateLineMarkerProvider extends RelatedItemLineMarkerProvider {
	@Override
	protected void collectNavigationMarkers(
		@NotNull PsiElement element,
		@NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result
	) {
		// This must be an element with a literal expression as a parent
		if (!(element instanceof PsiJavaTokenImpl) || !(element.getParent() instanceof PsiLiteralExpression)) return;

		// The literal expression must start with the StringTemplate language literal expression
		PsiLiteralExpression literalExpression = (PsiLiteralExpression) element.getParent();
		String value = literalExpression.getValue() instanceof String ? (String) literalExpression.getValue() : null;
		if ((value == null) || !value.startsWith(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR))
			return;

		// Get the StringTemplate language property usage
		Project project = element.getProject();
		String possibleProperties = value.substring(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR.length() + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR.length());
		final List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project, possibleProperties);
		if (properties.size() > 0) {
			// Add the property to a collection of line marker info
			NavigationGutterIconBuilder<PsiElement> builder =
				NavigationGutterIconBuilder.create(StringTemplateIcons.FILE)
					.setTargets(properties)
					.setTooltipText("Navigate to StringTemplate language property");
			result.add(builder.createLineMarkerInfo(element));
		}
	}

}