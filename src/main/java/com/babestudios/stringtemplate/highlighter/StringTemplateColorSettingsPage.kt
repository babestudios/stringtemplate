package com.babestudios.stringtemplate.highlighter

import com.babestudios.stringtemplate.StringTemplateIcons
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.options.colors.AttributesDescriptor
import com.intellij.openapi.options.colors.ColorDescriptor
import com.intellij.openapi.options.colors.ColorSettingsPage
import javax.swing.Icon

class StringTemplateColorSettingsPage : ColorSettingsPage {

	override fun getAttributeDescriptors(): Array<AttributesDescriptor> {
		return arrayOf(
			AttributesDescriptor("Conditional", StringTemplateSyntaxHighlighter.CONDITIONAL),
			AttributesDescriptor("Model formatter", StringTemplateSyntaxHighlighter.MODEL_FORMATTER),
			AttributesDescriptor("Model statement", StringTemplateSyntaxHighlighter.MODEL_STATEMENT),
			AttributesDescriptor("Comment", StringTemplateSyntaxHighlighter.COMMENT),
			AttributesDescriptor("Key", StringTemplateSyntaxHighlighter.KEY),
			AttributesDescriptor("Separator", StringTemplateSyntaxHighlighter.SEPARATOR),
			AttributesDescriptor("Value", StringTemplateSyntaxHighlighter.VALUE),
			AttributesDescriptor("Bad value", StringTemplateSyntaxHighlighter.BAD_CHARACTER)
		)
	}

	override fun getIcon(): Icon? {
		return StringTemplateIcons.FILE
	}

	override fun getHighlighter(): SyntaxHighlighter {
		return StringTemplateSyntaxHighlighter()
	}

	private val dollarSign = "\$"
	override fun getDemoText(): String {
		return """# You are reading the ".st" entry.
package aaaa.${dollarSign}model.aaaaaa$.asasda.asss

import ${dollarSign}model.aaaaaa;format="lowercase"${dollarSign}nnn"""
	}

	override fun getAdditionalHighlightingTagToDescriptorMap(): Map<String, TextAttributesKey>? {
		return null
	}

	override fun getColorDescriptors(): Array<ColorDescriptor> {
		return ColorDescriptor.EMPTY_ARRAY
	}

	override fun getDisplayName(): String {
		return "StringTemplate"
	}

}