package com.babestudios.stringtemplate.highlighter

import com.babestudios.stringtemplate.lexer.StringTemplateLexerAdapter
import com.babestudios.stringtemplate.psi.StringTemplateTypes
import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors.KEYWORD
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors.LINE_COMMENT
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors.OPERATION_SIGN
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors.STRING
import com.intellij.openapi.editor.HighlighterColors.NO_HIGHLIGHTING
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.TokenType
import com.intellij.psi.tree.IElementType

class StringTemplateSyntaxHighlighter : SyntaxHighlighterBase() {

	override fun getHighlightingLexer(): Lexer {
		return StringTemplateLexerAdapter()
	}

	override fun getTokenHighlights(tokenType: IElementType): Array<out TextAttributesKey?> {
		return when (tokenType) {
			StringTemplateTypes.SEPARATOR -> SEPARATOR_KEYS
			StringTemplateTypes.KEY -> KEY_KEYS
			StringTemplateTypes.VALUE -> VALUE_KEYS
			StringTemplateTypes.CONDITIONAL -> CONDITIONAL_KEYS
			StringTemplateTypes.CONDITIONAL_IF -> CONDITIONAL_KEYS
			StringTemplateTypes.CONDITIONAL_IF_START -> CONDITIONAL_KEYS
			StringTemplateTypes.CONDITIONAL_IF_END -> CONDITIONAL_KEYS
			StringTemplateTypes.MODEL_STATEMENT -> MODEL_STATEMENT_KEYS
			StringTemplateTypes.MODEL_FORMATTER -> MODEL_FORMATTER_KEYS
			StringTemplateTypes.MODEL_CLOSING -> MODEL_STATEMENT_KEYS
			StringTemplateTypes.COMMENT -> COMMENT_KEYS
			TokenType.BAD_CHARACTER -> BAD_CHAR_KEYS
			else -> EMPTY_KEYS
		}
	}

	companion object {

		val SEPARATOR: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_SEPARATOR", OPERATION_SIGN)
		val KEY: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_KEY", KEYWORD)
		val VALUE: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_VALUE", STRING)
		val CONDITIONAL: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_CONDITIONAL", KEYWORD)
		val MODEL_STATEMENT: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_CONDITIONAL_STATEMENT", STRING)
		val MODEL_FORMATTER: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_FORMATTER", KEYWORD)
		val COMMENT: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_COMMENT", LINE_COMMENT)
		val BAD_CHARACTER: TextAttributesKey = createTextAttributesKey("STRING_TEMPLATE_BAD_CHARACTER", NO_HIGHLIGHTING)

		private val BAD_CHAR_KEYS = arrayOf(BAD_CHARACTER)
		private val SEPARATOR_KEYS = arrayOf(SEPARATOR)
		private val KEY_KEYS = arrayOf(KEY)
		private val VALUE_KEYS = arrayOf(VALUE)
		private val COMMENT_KEYS = arrayOf(COMMENT)
		private val CONDITIONAL_KEYS = arrayOf(CONDITIONAL)
		private val MODEL_STATEMENT_KEYS = arrayOf(MODEL_STATEMENT)
		private val MODEL_FORMATTER_KEYS = arrayOf(MODEL_FORMATTER)
		private val EMPTY_KEYS = arrayOfNulls<TextAttributesKey>(0)
	}
}