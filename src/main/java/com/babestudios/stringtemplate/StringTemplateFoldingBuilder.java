package com.babestudios.stringtemplate;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.*;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.project.*;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.*;

import java.util.*;

public class StringTemplateFoldingBuilder extends FoldingBuilderEx implements DumbAware {
  @NotNull
  @Override
  public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {
    // Initialize the group of folding regions that will expand/collapse together.
    FoldingGroup group = FoldingGroup.newGroup(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR);
    // Initialize the list of folding regions
    List< FoldingDescriptor > descriptors = new ArrayList< FoldingDescriptor >();
    // Get a collection of the literal expressions in the document below root
    Collection< PsiLiteralExpression > literalExpressions =
          PsiTreeUtil.findChildrenOfType(root, PsiLiteralExpression.class);
    // Evaluate the collection
    for ( final PsiLiteralExpression literalExpression : literalExpressions ) {
      String value = literalExpression.getValue() instanceof String ? (String) literalExpression.getValue() : null;
      if ( value != null && value.startsWith(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR) ) {
        Project project = literalExpression.getProject();
        String key = value.substring(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR.length() + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR.length());
        // Get a list of all properties for a given key in the project
        final List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project, key);
        if ( properties.size() == 1 ) {
          // Add a folding descriptor for the literal expression at this node.
          descriptors.add(new FoldingDescriptor(literalExpression.getNode(),
                                                new TextRange(literalExpression.getTextRange().getStartOffset() + 1,
                                                              literalExpression.getTextRange().getEndOffset() - 1),
                                                group) );
        }
      }
    }
    return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
  }
  
  /**
   * Gets the StringTemplate Language 'value' string corresponding to the 'key'
   * @param node  Node corresponding to PsiLiteralExpression containing a string in the format
   *              STRING_TEMPLATE__PREFIX_STR + STRING_TEMPLATE__SEPARATOR_STR + Key, where Key is
   *              defined by the StringTemplate language file.
   */
  @Nullable
  @Override
  public String getPlaceholderText(@NotNull ASTNode node) {
    String retTxt = "...";
    if ( node.getPsi() instanceof PsiLiteralExpression ) {
      PsiLiteralExpression nodeElement = (PsiLiteralExpression) node.getPsi();
      String key = ((String) nodeElement.getValue()).substring(StringTemplateAnnotator.STRING_TEMPLATE_PREFIX_STR.length() + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR.length());
      final List< StringTemplateProperty > properties = StringTemplateUtil.findProperties(nodeElement.getProject(), key);
      String place = properties.get(0).getValue();
      // IMPORTANT: keys can come with no values, so a test for null is needed
      // IMPORTANT: Convert embedded \n to backslash n, so that the string will look
      // like it has LF embedded in it and embedded " to escaped "
      return place == null ? retTxt : place.replaceAll("\n", "\\n").replaceAll("\"", "\\\\\"");
    }
    return retTxt;
  }
  
  @Override
  public boolean isCollapsedByDefault(@NotNull ASTNode node) {
    return true;
  }
}