package com.babestudios.stringtemplate;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class StringTemplateFileType extends LanguageFileType {
  public static final StringTemplateFileType INSTANCE = new StringTemplateFileType();

  private StringTemplateFileType() {
    super(StringTemplateLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "StringTemplate file";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "StringTemplate language file";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "st";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return StringTemplateIcons.FILE;
  }
}