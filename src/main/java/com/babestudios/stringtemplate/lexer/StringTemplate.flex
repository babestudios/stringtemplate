package com.babestudios.stringtemplate;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;
import com.intellij.util.containers.Stack;

import static com.intellij.psi.TokenType.WHITE_SPACE;
import static com.babestudios.stringtemplate.psi.StringTemplateTypes.*;

%%

//https://jflex.de/manual.html#Example

%class StringTemplateLexer
%public
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

%{
	private Stack<Integer> stack = new Stack<>();

	public void yypushState(int newState) {
		System.out.println("push " + newState);
		stack.push(yystate());
		yybegin(newState);
	}

	public void yypopState() {
		System.out.println("pop " + stack.peek());
		yybegin(stack.pop());
	}
%}

CONDITIONAL="\$endif\$" | "\$else\$"
CONDITIONAL_START="\$if("
CONDITIONAL_FINISH="\)\$"
CONDITIONAL_MODEL_STATEMENT="model."[^);]+
MODEL_STATEMENT="\$model."[^\$;]+
MODEL_FORMATTER=";format="\"[^\"]*?\"
MODEL_CLOSING="\$"
CRLF=\n
WHITE_SPACE=[\ \n\t\f]
FIRST_VALUE_CHARACTER=[^ \n\f\\] | "\\"{CRLF} | "\\".
VALUE_CHARACTER=[^\n\f\\] | "\\"{CRLF} | "\\".
END_OF_LINE_COMMENT=("//")[^\r\n]*
BLOCK_COMMENT=("/*")[\s\S]*("*/")
SEPARATOR=[:=]"SEPARATOR"
LBRACE="{"
LPAREN = "("
RBRACE="}"
RPAREN = ")"
LBRACK = "["
RBRACK = "]"
COLON = ":"
COMMA = ","
LT = "<"
GT = ">"
AT = "@"
KEY_CHARACTER=[^:=\ \n\t\f\\] | "\\ "
DOT_QUALIFIED_EXPRESSION_PART_LITERAL=[^$\s][^\$\n]*
CLASS_IDENTIFIER=[^@{($\s]*
FUNCTION_IDENTIFIER=[^@(:;,{}$<>=\s\"][^@(:;,{}$<>=\s\"]*
PARAMETER_IDENTIFIER=[^@(:;,{}$)\s][^@(:;,{}$)\s]*
STRING_LITERAL=\"[^\"]\"
INTEGER_CONSTANT= 0 | [1-9][0-9]*
LONG_CONSTANT=0 | [1-9][0-9]*L
FLOAT_CONSTANT=[-\.0-9]+f
HEXA_CONSTANT=0x[a-fA-F0-9]+

%state PACKAGE_DIRECTIVE
%state IMPORT_DIRECTIVE
%state ANNOTATION
%state CLASS, ENUM, DATA_CLASS, OBJECT
%state CONSTRUCTOR, SUPER_TYPE_LIST
%state CLASS_BODY, ENUM_BODY
%state ENUM_ENTRIES
%state FUNCTION, FUNCTION_BODY, LAMBDA
%state EXPRESSION, WHEN, WHEN_BODY
%state FUNCTION_PARAMS, CLASS_PARAMS, ARGUMENTS
//Dot qualified Expression
%xstate DQE
%state CONDITIONAL_WITHIN
%state MODEL_WITHIN
%state WAITING_VALUE

%%

"->"																{ return T_ARROW; }
"by"																{ return KW_BY; }
"class"																{ if(yystate() == ANNOTATION) yypopState(); yypushState(CLASS); return KW_CLASS; }
"companion"															{ return KW_COMPANION; }
"data"																{ return KW_DATA; }
"get"																{ return KW_GET; }
"set"																{ return KW_SET; }
"interface"															{ if(yystate() == ANNOTATION) yypopState(); yypushState(CLASS); return KW_INTERFACE; }
"internal"															{ return KW_INTERNAL; }
"object"															{ yypushState(OBJECT); return KW_OBJECT; }
"open"																{ return KW_OPEN; }
"override"															{ if(yystate() == EXPRESSION || yystate() == ANNOTATION) yypopState(); return KW_OVERRIDE; }
"private"															{ return KW_PRIVATE; }
"sealed"															{ return KW_SEALED; }
"suspend"															{ return KW_SUSPEND; }
"val"																{ return KW_VAL; }
"var"																{ return KW_VAR; }
"when"																{ yypushState(WHEN); return KW_WHEN; }
{END_OF_LINE_COMMENT}												{ return COMMENT; }
{BLOCK_COMMENT}														{ return COMMENT; }
{CONDITIONAL}														{ return CONDITIONAL; }
{CONDITIONAL_START}													{ yypushState(CONDITIONAL_WITHIN); return CONDITIONAL_IF_START; }

<YYINITIAL> {
	{WHITE_SPACE}			{ return WHITE_SPACE; }

	"identifier"			{ return IDENTIFIER; }
	"new line"				{ return NL; }
	"}"						{ return T_RBRACE; }
	"("						{ yypushState(ARGUMENTS); return T_LPAREN; }
	")"						{ return T_RPAREN; }
	"["						{ return T_LBRACK; }
	"]"						{ return T_RBRACK; }
	"{"						{ return T_LBRACE; }
	":"						{ return T_COLON; }
	","						{ return T_COMMA; }
	"="						{ return T_EQ; }
	"@"						{ yypushState(ANNOTATION); return T_AT; }
	"abstract"				{ return KW_ABSTRACT; }
	"as"					{ return KW_AS; }
	"break"					{ return KW_BREAK; }
	"catch"					{ return KW_CATCH; }
	"const"					{ return KW_CONST; }
	"continue"				{ return KW_CONTINUE; }
	"do"					{ return KW_DO; }
	"else"					{ return KW_ELSE; }
	"enum"					{ yypushState(ENUM); return KW_ENUM; }
	"false"					{ return KW_FALSE; }
	"final"					{ return KW_FINAL; }
	"finally"				{ return KW_FINALLY; }
	"for"					{ return KW_FOR; }
	"if"					{ return KW_IF; }
	"import"				{ yybegin(IMPORT_DIRECTIVE); return KW_IMPORT; }
	"in"					{ return KW_IN; }
	"null"					{ return KW_NULL; }
	"package"				{ yybegin(PACKAGE_DIRECTIVE); return KW_PACKAGE; }
	"private"				{ return KW_PRIVATE; }
	"protected"				{ return KW_PROTECTED; }
	"public"				{ return KW_PUBLIC; }
	"return"				{ return KW_RETURN; }
	"static"				{ return KW_STATIC; }
	"super"					{ return KW_SUPER; }
	"synchronized"			{ return KW_SYNCHRONIZED; }
	"this"					{ return KW_THIS; }
	"throw"					{ return KW_THROW; }
	"transient"				{ return KW_TRANSIENT; }
	"true"					{ return KW_TRUE; }
	"try"					{ return KW_TRY; }
	"while"					{ return KW_WHILE; }

//kotlin additions
	"fun"					{ yypushState(FUNCTION); return KW_FUN; }
	"typealias"				{ return KW_TYPE_ALIAS; }

	{INTEGER_CONSTANT}												{ return INTEGER_CONSTANT; }
	{LONG_CONSTANT}													{ return LONG_CONSTANT; }
	{FLOAT_CONSTANT}												{ return FLOAT_CONSTANT; }
	{HEXA_CONSTANT}													{ return HEXA_CONSTANT; }
	{CLASS_IDENTIFIER}												{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
}

<PACKAGE_DIRECTIVE, IMPORT_DIRECTIVE> {
	{DOT_QUALIFIED_EXPRESSION_PART_LITERAL}							{ yybegin(DQE); return DOT_QUALIFIED_EXPRESSION_PART_LITERAL; }
	{MODEL_STATEMENT}												{ yybegin(DQE); yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
}
<DQE> {
	({CRLF}|{WHITE_SPACE})+											{ yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	{DOT_QUALIFIED_EXPRESSION_PART_LITERAL}							{ return DOT_QUALIFIED_EXPRESSION_PART_LITERAL; }
}

<ANNOTATION> {
	"fun"															{ yypopState(); yypushState(FUNCTION); return KW_FUN; }
	"("																{ yypushState(ARGUMENTS); return T_LPAREN; }
	"@"																{ return T_AT; }
	{CLASS_IDENTIFIER}												{ return IDENTIFIER; }
}

<CLASS, OBJECT> {
	{LBRACE}														{ yypushState(CLASS_BODY); return T_LBRACE; }
	"enum"															{ yypopState(); yypushState(ENUM); return KW_ENUM; }

	<ENUM, DATA_CLASS> {
//Class definition ends here
		{LPAREN}													{ yypushState(CLASS_PARAMS); return T_LPAREN; }
		{RBRACE}													{ yypopState(); if(yystate() == CLASS) yypopState(); return T_RBRACE; }
		"fun"														{ yypopState(); yypushState(FUNCTION); return KW_FUN; }
		"constructor"												{ yypushState(CONSTRUCTOR); return KW_CONSTRUCTOR; }
		":"															{ yypushState(SUPER_TYPE_LIST); return T_COLON; }
		{AT}														{ return T_AT; }
		{MODEL_STATEMENT}											{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
		{CLASS_IDENTIFIER}											{ return IDENTIFIER; }
	}

}

<ENUM> {
	{LBRACE}														{ yypushState(ENUM_BODY); return T_LBRACE; }
}

<CLASS_BODY> {
	"constructor"													{ yypushState(CONSTRUCTOR); return KW_CONSTRUCTOR; }
      	"("						{ return T_LPAREN; }
      	")"						{ return T_RPAREN; }
	<ENUM_BODY> {
		":"															{ return T_COLON; }
		"="															{ yypushState(EXPRESSION); return T_EQ; }
		"fun"														{ yypushState(FUNCTION); return KW_FUN; }
		{LT}														{ return T_LT; }
		{GT}														{ return T_GT; }
		{COMMA}														{ return T_COMMA; }
		{MODEL_STATEMENT}											{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
		{RBRACE}													{
																		yypopState();
																		if(yystate() == OBJECT || yystate() == CLASS) yypopState();
																		if(yystate() == EXPRESSION) yypopState();
																		if(yystate() == FUNCTION) yypopState();
																		return T_RBRACE;
																	}
		{FUNCTION_IDENTIFIER}										{ return IDENTIFIER; }
	}
}

<CONSTRUCTOR> {
	":"																{ yypopState(); yypushState(SUPER_TYPE_LIST); return T_COLON; }
	<SUPER_TYPE_LIST> {
		{LT}														{ return T_LT; }
		{GT}														{ return T_GT; }
		{COMMA}														{ return T_COMMA; }
		{MODEL_STATEMENT}											{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
		{LPAREN}													{ yypushState(CLASS_PARAMS); return T_LPAREN; }
		{LBRACE}													{ yypopState(); yypushState(CLASS_BODY); return T_LBRACE; }
		{RBRACE}													{ yypopState(); if(yystate() == CLASS_BODY) yypopState(); return T_RBRACE; }
		{FUNCTION_IDENTIFIER}										{ return IDENTIFIER; }
	}
}

<SUPER_TYPE_LIST> {
	{RPAREN}														{ yypopState(); return T_RPAREN; }
}

<ENUM_BODY> {
	{RBRACE}														{ yypopState(); return T_RBRACE; }
	{FUNCTION_IDENTIFIER}											{ yypushState(ENUM_ENTRIES); return IDENTIFIER; }
}

<ENUM_ENTRIES> {
	{STRING_LITERAL}												{ return STRING_LITERAL; }
	";"																{ yypopState(); return T_SEMICOLON; }
	{LPAREN}														{ return T_LPAREN; }
	{RPAREN}														{ return T_RPAREN; }
	{COMMA}															{ return T_COMMA; }
	{FUNCTION_IDENTIFIER}											{ return IDENTIFIER; }
}

<FUNCTION> {
	"fun"															{ return KW_FUN; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	"="																{ yypushState(EXPRESSION); return T_EQ; }
	{LPAREN}														{ yypushState(FUNCTION_PARAMS); return T_LPAREN; }
	{COLON}															{ return T_COLON; }
	{LT}															{ return T_LT; }
	{GT}															{ return T_GT; }
	{LBRACE}														{ yypushState(FUNCTION_BODY); return T_LBRACE; }
	{FUNCTION_IDENTIFIER}											{ return IDENTIFIER; }
}

<ARGUMENTS> {
	"::"															{ return T_COLONCOLON; }
	"="																{ return T_EQ; }
	{LPAREN}														{ yypushState(ARGUMENTS); return T_LPAREN; }
	"?:"															{ return T_ELVIS; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{STRING_LITERAL}												{ return STRING_LITERAL; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	{COMMA}															{ return T_COMMA; }
	{RPAREN}														{ yypopState(); return T_RPAREN; }
}

<EXPRESSION> {
	"fun"															{ yypopState(); yypopState(); yypushState(FUNCTION); return KW_FUN; }
	"?:"															{ return T_ELVIS; }
	{COLON}															{ return T_COLON; }
	{STRING_LITERAL}												{ return STRING_LITERAL; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{RBRACE}														{ yypopState(); if(yystate() == CLASS_BODY) yypopState(); return T_RBRACE; }
	{LPAREN}														{ yypushState(ARGUMENTS); return T_LPAREN; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
}

<WHEN> {
	{LBRACE}														{ yypushState(WHEN_BODY); return T_LBRACE; }
	{LPAREN}														{ return T_LPAREN; }
	{RPAREN}														{ return T_RPAREN; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
}

<WHEN_BODY> {
	"is"															{ return KW_IS; }
	"->"															{ return T_ARROW; }
	"?:"															{ return T_ELVIS; }
	{STRING_LITERAL}												{ return STRING_LITERAL; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{RBRACE}														{ yypopState(); yypopState(); if(yystate() == EXPRESSION) yypopState(); if(yystate() == FUNCTION) yypopState(); return T_RBRACE; }
	{LPAREN}														{ yypushState(ARGUMENTS); return T_LPAREN; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
}

<CLASS_PARAMS, FUNCTION_PARAMS> {
	"val"															{ return KW_VAL; }
	"="																{ return T_EQ; }
//Double this state for function references
	{LPAREN}														{ yypushState(FUNCTION_PARAMS); return T_LPAREN; }
	"->"															{ return T_ARROW; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{LT}															{ return T_LT; }
	{GT}															{ return T_GT; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{AT}															{ return T_AT; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	{COMMA}															{ return T_COMMA; }
	{COLON}															{ return T_COLON; }
	{RPAREN}														{ yypopState(); if(yystate() == SUPER_TYPE_LIST) yypopState(); return T_RPAREN; }
}

<LAMBDA> {
	"->"															{ return T_ARROW; }
	{COMMA}															{ return T_COMMA; }
	{LPAREN}														{ yypushState(ARGUMENTS); return T_LPAREN; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{PARAMETER_IDENTIFIER}											{ return IDENTIFIER; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	{RBRACE}														{ yypopState(); if(yystate() == EXPRESSION) { yypopState(); if(yystate() == FUNCTION) yypopState();}; return T_RBRACE; }
}

<FUNCTION_BODY> {
	"fun"															{ yypopState(); yypushState(FUNCTION); return KW_FUN; }
	"return"														{ yypushState(EXPRESSION); return KW_RETURN; }
	"="																{ yypushState(EXPRESSION); return T_EQ; }
	{LPAREN}														{ yypushState(ARGUMENTS); return T_LPAREN; }
	{MODEL_STATEMENT}												{ yypushState(MODEL_WITHIN); return MODEL_STATEMENT; }
	{LBRACE}														{ yypushState(LAMBDA); return T_LBRACE; }
	{RBRACE}														{ yypopState(); if(yystate() != YYINITIAL) yypopState(); return T_RBRACE; }
	{FUNCTION_IDENTIFIER}											{ return IDENTIFIER; }
}

<CONDITIONAL_WITHIN> {
	{CONDITIONAL_MODEL_STATEMENT}									{ return MODEL_STATEMENT; }
	{CONDITIONAL_FINISH}											{ yypopState(); return CONDITIONAL_IF_END; }
}
<MODEL_WITHIN> {
	{MODEL_FORMATTER}												{ return MODEL_FORMATTER; }
	{MODEL_CLOSING}													{ yypopState(); return MODEL_CLOSING; }
}
/*<YYINITIAL> {SEPARATOR}												     { yybegin(WAITING_VALUE); return SEPARATOR; } */

<WAITING_VALUE> {CRLF}({CRLF}|{WHITE_SPACE})+						    { yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }

<WAITING_VALUE> {WHITE_SPACE}+												{ yybegin(WAITING_VALUE); return TokenType.WHITE_SPACE; }

/*<WAITING_VALUE> {FIRST_VALUE_CHARACTER}{VALUE_CHARACTER}*         { yybegin(YYINITIAL); return VALUE; }*/

({CRLF}|{WHITE_SPACE})+												       { return TokenType.WHITE_SPACE; }

[^]																		        { return TokenType.BAD_CHARACTER; }
