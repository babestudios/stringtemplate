package com.babestudios.stringtemplate.lexer

import com.babestudios.stringtemplate.StringTemplateLexer
import com.intellij.lexer.FlexAdapter
import java.io.Reader

class StringTemplateLexerAdapter : FlexAdapter(StringTemplateLexer(null as Reader?))
