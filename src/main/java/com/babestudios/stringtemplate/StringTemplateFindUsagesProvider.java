package com.babestudios.stringtemplate;

import com.babestudios.stringtemplate.lexer.StringTemplateLexerAdapter;
import com.babestudios.stringtemplate.psi.StringTemplateTypes;
import com.intellij.lang.cacheBuilder.*;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.psi.*;
import com.intellij.psi.tree.TokenSet;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.*;

public class StringTemplateFindUsagesProvider implements FindUsagesProvider {
  @Nullable
  @Override
  public WordsScanner getWordsScanner() {
    return new DefaultWordsScanner(new StringTemplateLexerAdapter(),
                                   TokenSet.create(StringTemplateTypes.KEY),
                                   TokenSet.create(StringTemplateTypes.COMMENT),
                                   TokenSet.EMPTY);
  }
  
  @Override
  public boolean canFindUsagesFor(@NotNull PsiElement psiElement) {
    return psiElement instanceof PsiNamedElement;
  }
  
  @Nullable
  @Override
  public String getHelpId(@NotNull PsiElement psiElement) {
    return null;
  }
  
  @NotNull
  @Override
  public String getType(@NotNull PsiElement element) {
    if (element instanceof StringTemplateProperty) {
      return "StringTemplate property";
    } else {
      return "";
    }
  }
  
  @NotNull
  @Override
  public String getDescriptiveName(@NotNull PsiElement element) {
    if (element instanceof StringTemplateProperty) {
      return ((StringTemplateProperty) element).getKey();
    } else {
      return "";
    }
  }
  
  @NotNull
  @Override
  public String getNodeText(@NotNull PsiElement element, boolean useFullName) {
    if (element instanceof StringTemplateProperty) {
      return ((StringTemplateProperty) element).getKey() + StringTemplateAnnotator.STRING_TEMPLATE_SEPARATOR_STR + ((StringTemplateProperty) element).getValue();
    } else {
      return "";
    }
  }
}