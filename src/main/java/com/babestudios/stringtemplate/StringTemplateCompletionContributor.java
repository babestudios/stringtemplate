package com.babestudios.stringtemplate;

import com.babestudios.stringtemplate.psi.StringTemplateTypes;
import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

public class StringTemplateCompletionContributor extends CompletionContributor {
  public StringTemplateCompletionContributor() {
    extend( CompletionType.BASIC,
            PlatformPatterns.psiElement(StringTemplateTypes.VALUE).withLanguage(StringTemplateLanguage.INSTANCE),
            new CompletionProvider<CompletionParameters>() {
                  public void addCompletions(@NotNull CompletionParameters parameters,
                                             ProcessingContext context,
                                             @NotNull CompletionResultSet resultSet) {
                    resultSet.addElement(LookupElementBuilder.create("Hello"));
                  }
            }
    );
  }
}
