package com.babestudios.stringtemplate;

import com.intellij.lang.refactoring.RefactoringSupportProvider;
import com.intellij.psi.PsiElement;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.*;

public class StringTemplateRefactoringSupportProvider extends RefactoringSupportProvider {
  @Override
  public boolean isMemberInplaceRenameAvailable(@NotNull PsiElement elementToRename, @Nullable PsiElement context) {
    return (elementToRename instanceof StringTemplateProperty);
  }
}
