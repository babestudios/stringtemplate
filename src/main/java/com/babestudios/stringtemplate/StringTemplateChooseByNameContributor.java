package com.babestudios.stringtemplate;

import com.intellij.navigation.*;
import com.intellij.openapi.project.Project;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class StringTemplateChooseByNameContributor implements ChooseByNameContributor {
  @NotNull
  @Override
  public String[] getNames(Project project, boolean includeNonProjectItems) {
    List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project);
    List<String> names = new ArrayList<String>(properties.size());
    for (StringTemplateProperty property : properties) {
      if (property.getKey() != null && property.getKey().length() > 0) {
        names.add(property.getKey());
      }
    }
    return names.toArray(new String[names.size()]);
  }
  
  @NotNull
  @Override
  public NavigationItem[] getItemsByName(String name, String pattern, Project project, boolean includeNonProjectItems) {
    // TODO: include non project items
    List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project, name);
    return properties.toArray(new NavigationItem[properties.size()]);
  }
}