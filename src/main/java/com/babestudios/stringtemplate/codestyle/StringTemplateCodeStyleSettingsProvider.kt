package com.babestudios.stringtemplate.codestyle

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.application.options.*
import com.intellij.lang.Language
import com.intellij.psi.codeStyle.CodeStyleConfigurable
import com.intellij.psi.codeStyle.CodeStyleSettings
import com.intellij.psi.codeStyle.CodeStyleSettingsProvider
import com.intellij.psi.codeStyle.CustomCodeStyleSettings

class StringTemplateCodeStyleSettingsProvider : CodeStyleSettingsProvider() {

	override fun getLanguage(): Language {
		return StringTemplateLanguage
	}

	override fun createCustomSettings(settings: CodeStyleSettings): CustomCodeStyleSettings {
		return StringTemplateCodeStyleSettings(settings)
	}

	override fun getConfigurableDisplayName(): String {
		return "StringTemplate"
	}

	override fun createConfigurable(
		settings: CodeStyleSettings,
		modelSettings: CodeStyleSettings
	): CodeStyleConfigurable {

		return object : CodeStyleAbstractConfigurable(settings, modelSettings, this.configurableDisplayName) {
			override fun createPanel(settings: CodeStyleSettings): CodeStyleAbstractPanel {
				return object : TabbedLanguageCodeStylePanel(StringTemplateLanguage, settings, modelSettings) {}
			}
		}

	}

}
