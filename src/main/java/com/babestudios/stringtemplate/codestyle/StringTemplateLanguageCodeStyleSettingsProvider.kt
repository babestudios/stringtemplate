package com.babestudios.stringtemplate.codestyle

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.application.options.CodeStyleAbstractConfigurable
import com.intellij.application.options.CodeStyleAbstractPanel
import com.intellij.application.options.IndentOptionsEditor
import com.intellij.application.options.SmartIndentOptionsEditor
import com.intellij.lang.Language
import com.intellij.psi.codeStyle.CodeStyleConfigurable
import com.intellij.psi.codeStyle.CodeStyleSettings
import com.intellij.psi.codeStyle.CodeStyleSettingsCustomizable
import com.intellij.psi.codeStyle.LanguageCodeStyleSettingsProvider

class StringTemplateLanguageCodeStyleSettingsProvider : LanguageCodeStyleSettingsProvider() {

	override fun getLanguage(): Language {
		return StringTemplateLanguage
	}

	override fun getLanguageName(): String {
		return "StringTemplate"
	}

	override fun getIndentOptionsEditor(): IndentOptionsEditor = SmartIndentOptionsEditor()

	override fun customizeSettings(consumer: CodeStyleSettingsCustomizable, settingsType: SettingsType) {
		when (settingsType) {
			SettingsType.SPACING_SETTINGS -> {
				consumer.showStandardOptions("SPACE_AFTER_COMMA")
				consumer.showStandardOptions("SPACE_AROUND_ASSIGNMENT_OPERATORS")
				consumer.renameStandardOption("SPACE_AROUND_ASSIGNMENT_OPERATORS", "Around equals")

				consumer.showStandardOptions("SPACE_AROUND_LOGICAL_OPERATORS")
				consumer.renameStandardOption("SPACE_AROUND_LOGICAL_OPERATORS", "Space around logical operators")
				//  consumer.renameStandardOption("SPACE_AROUND_ASSIGNMENT_OPERATORS", "Separator");
			}

			SettingsType.BLANK_LINES_SETTINGS -> {
				 consumer.showStandardOptions("KEEP_BLANK_LINES_IN_CODE");
			}

			SettingsType.WRAPPING_AND_BRACES_SETTINGS -> {
				 consumer.showStandardOptions("KEEP_LINE_BREAKS");
			}

			SettingsType.INDENT_SETTINGS -> {
//				consumer.showStandardOptions("INDENT_SIZE")
//				consumer.showStandardOptions("TAB_SIZE")
//				consumer.showStandardOptions("USE_TAB_CHARACTER")
//				consumer.showStandardOptions("SMART_TABS")
			}

			SettingsType.COMMENTER_SETTINGS -> {
				consumer.showStandardOptions()
			}

			SettingsType.LANGUAGE_SPECIFIC -> {
				consumer.showStandardOptions()
			}
		}
	}

	override fun getCodeSample(settingsType: SettingsType): String {
		return """# You are reading the ".st" entry.
! The exclamation mark can also mark text as comments.
website = http://en.wikipedia.org/

language = English
# The backslash below tells the application to continue reading
# the value onto the next line.
message = Welcome to \
          Wikipedia!
# Add spaces to the key
key\ with\ spaces = This is the value that could be looked up with the key "key with spaces".
# Unicode
tab : \u0009"""
	}
}
