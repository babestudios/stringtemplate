package com.babestudios.stringtemplate.codestyle

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.application.options.GenerationCodeStylePanel
import com.intellij.application.options.TabbedLanguageCodeStylePanel
import com.intellij.psi.codeStyle.CodeStyleSettings
import com.intellij.psi.codeStyle.CodeStyleSettingsProvider

class StringTemplateCodeStyleMainPanel(currentSettings: CodeStyleSettings?, settings: CodeStyleSettings) :
	TabbedLanguageCodeStylePanel(StringTemplateLanguage, currentSettings, settings) {

//	override fun initTabs(settings: CodeStyleSettings) {
		//addSpacesTab(settings)
		//addBlankLinesTab(settings)
		//addIndentOptionsTab(settings)
		//addWrappingAndBracesTab(settings)
//		for (provider in CodeStyleSettingsProvider.EXTENSION_POINT_NAME.extensions) {
//			if (provider.language == StringTemplateLanguage && !provider.hasSettingsPage()) {
//				createTab(provider)
//			}
//		}
//	}
}
