package com.babestudios.stringtemplate.codestyle

import com.intellij.psi.codeStyle.CodeStyleSettings
import com.intellij.psi.codeStyle.CustomCodeStyleSettings

class StringTemplateCodeStyleSettings(settings: CodeStyleSettings?) :
	CustomCodeStyleSettings("StringTemplateCodeStyleSettings", settings!!)
