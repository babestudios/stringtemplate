package com.babestudios.stringtemplate.parser

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.babestudios.stringtemplate.lexer.StringTemplateLexerAdapter
import com.babestudios.stringtemplate.psi.StringTemplateFile
import com.babestudios.stringtemplate.psi.StringTemplateTypes
import com.intellij.lang.ASTNode
import com.intellij.lang.ParserDefinition
import com.intellij.lang.ParserDefinition.SpaceRequirements
import com.intellij.lang.PsiParser
import com.intellij.lexer.Lexer
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.TokenType
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet

val WHITE_SPACES: TokenSet = TokenSet.create(TokenType.WHITE_SPACE)
val COMMENTS: TokenSet = TokenSet.create(StringTemplateTypes.COMMENT)
val FILE: IFileElementType = IFileElementType(StringTemplateLanguage)

class StringTemplateParserDefinition : ParserDefinition {
	override fun createLexer(project: Project): Lexer {
		return StringTemplateLexerAdapter()
	}

	override fun getWhitespaceTokens(): TokenSet {
		return WHITE_SPACES
	}

	override fun getCommentTokens(): TokenSet {
		return COMMENTS
	}

	override fun getStringLiteralElements(): TokenSet {
		return TokenSet.EMPTY
	}

	override fun createParser(project: Project): PsiParser {
		return StringTemplateParser()
	}

	override fun getFileNodeType(): IFileElementType {
		return FILE
	}

	override fun createFile(viewProvider: FileViewProvider): PsiFile {
		return StringTemplateFile(viewProvider)
	}

	override fun spaceExistenceTypeBetweenTokens(left: ASTNode, right: ASTNode): SpaceRequirements {
		return SpaceRequirements.MAY
	}

	override fun createElement(node: ASTNode): PsiElement {
		return StringTemplateTypes.Factory.createElement(node)
	}

}
