package com.babestudios.stringtemplate

import com.babestudios.stringtemplate.highlighter.StringTemplateSyntaxHighlighter
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiLiteralExpression

class StringTemplateAnnotator : Annotator {
	override fun annotate(element: PsiElement, holder: AnnotationHolder) {
		// Ensure the Psi Element is an expression
		if (element !is PsiLiteralExpression) return

		// Ensure the Psi element contains a string that starts with the key and separator
		val literalExpression = element
		val value = if (literalExpression.value is String) literalExpression.value as String? else null
		if ((value == null) || !value.startsWith(STRING_TEMPLATE_PREFIX_STR + STRING_TEMPLATE_SEPARATOR_STR)) return

		// Define the text ranges (start is inclusive, end is exclusive)
		// "stringtemplate:key"
		//  01234567890
		val prefixRange = TextRange.from(element.getTextRange().startOffset, STRING_TEMPLATE_PREFIX_STR.length + 1)
		val separatorRange = TextRange.from(prefixRange.endOffset, STRING_TEMPLATE_SEPARATOR_STR.length)
		val keyRange = TextRange(separatorRange.endOffset, element.getTextRange().endOffset - 1)

		// Get the list of properties from the Project
		val possibleProperties =
			value.substring(STRING_TEMPLATE_PREFIX_STR.length + STRING_TEMPLATE_SEPARATOR_STR.length)
		val project = element.getProject()
		val properties = StringTemplateUtil.findProperties(project, possibleProperties)

		// Set the annotations using the text ranges.
		val keyAnnotation = holder.createInfoAnnotation(prefixRange, null)
		keyAnnotation.textAttributes = DefaultLanguageHighlighterColors.KEYWORD
		val separatorAnnotation = holder.createInfoAnnotation(separatorRange, null)
		separatorAnnotation.textAttributes = StringTemplateSyntaxHighlighter.SEPARATOR
		if (properties.isEmpty()) {
			// No well-formed property found following the key-separator
			val badProperty = holder.createErrorAnnotation(keyRange, "Unresolved property")
			badProperty.textAttributes = StringTemplateSyntaxHighlighter.BAD_CHARACTER
			// ** Tutorial step 18.3 - Add a quick fix for the string containing possible properties
			badProperty.registerFix(StringTemplateCreatePropertyQuickFix(possibleProperties))
		} else {
			// Found at least one property
			val annotation = holder.createInfoAnnotation(keyRange, null)
			annotation.textAttributes = StringTemplateSyntaxHighlighter.VALUE
		}
	}

	companion object {
		// Define strings for the StringTemplate language prefix - used for annotations, line markers, etc.
		const val STRING_TEMPLATE_PREFIX_STR: String = "stringtemplate"
		const val STRING_TEMPLATE_SEPARATOR_STR: String = ":"
	}
}
