package com.babestudios.stringtemplate.psi.impl

import com.babestudios.stringtemplate.StringTemplateIcons
import com.babestudios.stringtemplate.psi.StringTemplateElementFactory.createProperty
import com.babestudios.stringtemplate.psi.StringTemplateProperty
import com.babestudios.stringtemplate.psi.StringTemplateTypes
import com.intellij.navigation.ItemPresentation
import com.intellij.psi.PsiElement
import javax.swing.Icon

object StringTemplatePsiImplUtil {

	@JvmStatic
	fun getKey(element: StringTemplateProperty): String? {
		val keyNode = element.node.findChildByType(StringTemplateTypes.KEY)
		return keyNode?.text?.replace("\\\\ ".toRegex(), " ")
	}

	@JvmStatic
	fun getValue(element: StringTemplateProperty): String? {
		val valueNode = element.node.findChildByType(StringTemplateTypes.VALUE)
		return valueNode?.text
	}

	@JvmStatic
	fun getName(element: StringTemplateProperty): String? {
		return getKey(element)
	}

	@JvmStatic
	fun setName(element: StringTemplateProperty, newName: String?): PsiElement {
		val keyNode = element.node.findChildByType(StringTemplateTypes.KEY)
		if (keyNode != null) {
			val property = createProperty(element.project, newName)
			val newKeyNode = property.firstChild.node
			element.node.replaceChild(keyNode, newKeyNode)
		}
		return element
	}

	@JvmStatic
	fun getNameIdentifier(element: StringTemplateProperty): PsiElement? {
		val keyNode = element.node.findChildByType(StringTemplateTypes.KEY)
		return keyNode?.psi
	}

	@JvmStatic
	fun getPresentation(element: StringTemplateProperty): ItemPresentation {
		return object : ItemPresentation {
			override fun getPresentableText(): String? {
				return element.key
			}

			override fun getLocationString(): String? {
				val containingFile = element.containingFile
				return containingFile?.name
			}

			override fun getIcon(unused: Boolean): Icon? {
				return StringTemplateIcons.FILE
			}
		}
	}

}
