package com.babestudios.stringtemplate.psi.impl

import com.babestudios.stringtemplate.psi.StringTemplateNamedElement
import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode

abstract class StringTemplateNamedElementImpl(node: ASTNode) : ASTWrapperPsiElement(node), StringTemplateNamedElement