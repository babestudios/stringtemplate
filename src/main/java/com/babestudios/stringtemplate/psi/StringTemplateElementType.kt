package com.babestudios.stringtemplate.psi

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.psi.tree.IElementType
import org.jetbrains.annotations.NonNls

class StringTemplateElementType(debugName: @NonNls String) : IElementType(debugName, StringTemplateLanguage)