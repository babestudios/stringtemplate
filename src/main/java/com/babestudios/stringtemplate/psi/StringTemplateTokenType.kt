package com.babestudios.stringtemplate.psi

import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.psi.tree.IElementType
import org.jetbrains.annotations.NonNls

class StringTemplateTokenType(debugName: @NonNls String) : IElementType(debugName, StringTemplateLanguage) {
	override fun toString(): String {
		return "StringTemplateTokenType." + super.toString()
	}
}
