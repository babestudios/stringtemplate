package com.babestudios.stringtemplate.psi

import com.intellij.psi.PsiNameIdentifierOwner

interface StringTemplateNamedElement : PsiNameIdentifierOwner
