package com.babestudios.stringtemplate.psi

import com.babestudios.stringtemplate.StringTemplateFileType
import com.babestudios.stringtemplate.StringTemplateLanguage
import com.intellij.extapi.psi.PsiFileBase
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider

class StringTemplateFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, StringTemplateLanguage) {

	override fun getFileType(): FileType {
		return StringTemplateFileType.INSTANCE
	}

	override fun toString(): String {
		return "StringTemplate File"
	}

}
