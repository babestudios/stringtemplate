package com.babestudios.stringtemplate.psi

import com.babestudios.stringtemplate.StringTemplateFileType
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFileFactory

object StringTemplateElementFactory {

	@JvmStatic
	fun createProperty(project: Project?, name: String?): StringTemplateProperty {
		val file = createFile(project, name)
		return file.firstChild as StringTemplateProperty
	}

	private fun createFile(project: Project?, text: String?): StringTemplateFile {
		val name = "dummy.stringtemplate"
		return PsiFileFactory.getInstance(project)
			.createFileFromText(name, StringTemplateFileType.INSTANCE, text!!) as StringTemplateFile
	}

	@JvmStatic
	fun createProperty(project: Project?, name: String, value: String): StringTemplateProperty {
		val file = createFile(project, "$name = $value")
		return file.firstChild as StringTemplateProperty
	}

	@JvmStatic
	fun createCRLF(project: Project?): PsiElement {
		val file = createFile(project, "\n")
		return file.firstChild
	}

}
