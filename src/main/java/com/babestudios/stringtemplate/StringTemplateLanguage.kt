package com.babestudios.stringtemplate

import com.intellij.lang.Language

object StringTemplateLanguage : Language("StringTemplate") {
    private fun readResolve(): Any = StringTemplateLanguage
}
