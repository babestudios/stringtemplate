package com.babestudios.stringtemplate;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.babestudios.stringtemplate.psi.StringTemplateFile;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;

import java.util.*;

public class StringTemplateUtil {
  
  // Searches the entire project for StringTemplate language files with instances of the StringTemplate property with the given key
  public static List<StringTemplateProperty> findProperties(Project project, String key) {
    List<StringTemplateProperty> result = new ArrayList<>();
    Collection<VirtualFile> virtualFiles =
          FileTypeIndex.getFiles(StringTemplateFileType.INSTANCE, GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      StringTemplateFile stringTemplateFile = (StringTemplateFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (stringTemplateFile != null) {
        StringTemplateProperty[] properties = PsiTreeUtil.getChildrenOfType(stringTemplateFile, StringTemplateProperty.class);
        if (properties != null) {
          for (StringTemplateProperty property : properties) {
            if (key.equals(property.getKey())) {
               result.add(property);
            }
          }
        }
      }
    }
    return result;
  }
  
  public static List<StringTemplateProperty> findProperties(Project project) {
    List<StringTemplateProperty> result = new ArrayList<StringTemplateProperty>();
    Collection<VirtualFile> virtualFiles =
          FileTypeIndex.getFiles(StringTemplateFileType.INSTANCE, GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      StringTemplateFile stringTemplateFile = (StringTemplateFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (stringTemplateFile != null) {
        StringTemplateProperty[] properties = PsiTreeUtil.getChildrenOfType(stringTemplateFile, StringTemplateProperty.class);
        if (properties != null) {
          Collections.addAll(result, properties);
        }
      }
    }
    return result;
  }
}