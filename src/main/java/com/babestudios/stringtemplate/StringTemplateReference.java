package com.babestudios.stringtemplate;

import com.intellij.codeInsight.lookup.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.babestudios.stringtemplate.psi.StringTemplateProperty;
import org.jetbrains.annotations.*;

import java.util.*;

public class StringTemplateReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference {
  private String key;
  
  public StringTemplateReference(@NotNull PsiElement element, TextRange textRange) {
    super(element, textRange);
    key = element.getText().substring(textRange.getStartOffset(), textRange.getEndOffset());
  }
  
  @NotNull
  @Override
  public ResolveResult[] multiResolve(boolean incompleteCode) {
    Project project = myElement.getProject();
    final List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project, key);
    List<ResolveResult> results = new ArrayList<ResolveResult>();
    for (StringTemplateProperty property : properties) {
      results.add(new PsiElementResolveResult(property));
    }
    return results.toArray(new ResolveResult[results.size()]);
  }
  
  @Nullable
  @Override
  public PsiElement resolve() {
    ResolveResult[] resolveResults = multiResolve(false);
    return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
  }
  
  @NotNull
  @Override
  public Object[] getVariants() {
    Project project = myElement.getProject();
    List<StringTemplateProperty> properties = StringTemplateUtil.findProperties(project);
    List<LookupElement> variants = new ArrayList<LookupElement>();
    for (final StringTemplateProperty property : properties) {
      if (property.getKey() != null && property.getKey().length() > 0) {
        variants.add(LookupElementBuilder
                           .create(property).withIcon(StringTemplateIcons.FILE)
                           .withTypeText(property.getContainingFile().getName())
        );
      }
    }
    return variants.toArray();
  }
}
