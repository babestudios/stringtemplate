package com.babestudios.stringtemplate;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class StringTemplateIcons {
  public static final Icon FILE = IconLoader.getIcon("/icons/sun-glasses.png");
}