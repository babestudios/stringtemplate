# StringTemplate

This is an IntelliJ plugin for using StringTemplate with specific highlighting rules.

### References

* [Reference guide](https://www.jetbrains.org/intellij/sdk/docs/reference_guide/custom_language_support.html)
* [Tutorial with sample code](https://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support_tutorial.html)

### Samples

* [Simple language code sample](https://github.com/JetBrains/intellij-sdk-docs/tree/master/code_samples/simple_language_plugin)
* [Custom language plugins with code](https://plugins.jetbrains.com/search?headline=48-languages&tags=Languages)
* [Official StringTemplate plugin (could be used for inspiration)](https://plugins.jetbrains.com/plugin/8041-stringtemplate-v4)

